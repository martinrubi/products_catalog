<?php

namespace AppBundle\Controller\APIv1\Frontend\Products;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller
{
    /**
     * Frontend - Searches for products and returns a json with the found products.
     * The search parameter comes as a query parameter and is named 'q'.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listAction(Request $request)
    {
        $q = $request->get('q');

        $productsRepository =  $this->getDoctrine()
            ->getRepository(Product::class);

        $products = $productsRepository->searchContaining($q);

        return $this->json([
            'success' => true,
            'data' => $this->getProductsJson($products)
        ]);
    }

    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given $products array.
     */
    protected function getProductsJson($products)
    {
        return array_map(
            function($product) {
                return $this->getProductJson($product);
            },
            $products
        );
    }

    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given $product.
     */
    protected function getProductJson($product)
    {
        $category = $product->getCategory();

        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'image' => '/images/products/' . $product->getImage(),
            'category' => [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'icon' => '/images/categories/' . $category->getIcon(),
            ],
        ];
    }
}