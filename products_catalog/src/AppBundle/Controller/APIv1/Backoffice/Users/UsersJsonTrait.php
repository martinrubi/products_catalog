<?php

namespace AppBundle\Controller\APIv1\Backoffice\Users;

/**
 * Adds the methods to serialize users to JSON associative array.
 */
trait UsersJsonTrait
{
    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given $users array.
     */
    protected function getUsersJson ($users)
    {
        return array_map(
            function($user) {
                return $this->getUserJson($user);
            },
            $users
        );
    }

    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given $user.
     */
    protected function getUserJson($user)
    {
        return [
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
        ];
    }
}