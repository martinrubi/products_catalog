<?php

namespace AppBundle\Controller\APIv1\Backoffice\Users;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\User;
use Respect\Validation\Validator as v;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CreateController extends Controller
{
    use UsersJsonTrait;
    use ValidationTrait;

    /**
     * Return the validation for each attribute in the request.
     * @return array
     */
    protected function getValidators($usersRepository): array
    {
        v::with('\\AppBundle\\Validations\\');

        return [
            'username' => v::key('username', v::notOptional()->stringType()->length(1, 255)
                ->notBlank() ->doesNotExist($usersRepository, 'username')),
            'email' => v::key('email', v::notOptional()->stringType()->length(1, 255)
                ->notBlank()->email() ->doesNotExist($usersRepository, 'email')),
            'password' => v::key('password', v::notOptional()->stringType()->length(6, 255)
                ->notBlank() ),
        ];
    }

    /**
     * Creates a User.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function createAction(Request $request)
    {
        $usersRepository =  $this->getDoctrine()
            ->getRepository(User::class);

        $params = $request->get('user');

        if ( $validationErrors = $this->getValidationErrorsOn($params, $usersRepository) ) {
            return $this->validationErrorsJson($validationErrors);
        }

        $user = new User();
        $user->setUsername( $params['username'] );
        $user->setEmail( $params['email'] );
        $user->setPassword( $params['password'] );

        $usersRepository->persist($user);

        $this->getDoctrine()->getManager()->flush();

        return $this->json([
            'success' => true,
            'data' => $this->getUserJson($user)
        ]);
    }
}