<?php

namespace AppBundle\Controller\APIv1\Backoffice\Users;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\User;
use Respect\Validation\Validator as v;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UpdateController extends Controller
{
    use UsersJsonTrait;
    use ValidationTrait;

    /**
     * Return the validation for each attribute in the request.
     * @return array
     */
    protected function getValidators($user, $usersRepository, $params): array
    {
        v::with('\\AppBundle\\Validations\\');

        $validator = [
            'username' => v::key('username', v::notOptional()->stringType()->length(1, 255)
                ->notBlank() ->doesNotExist($usersRepository, 'username', $user)),
        ];

        if ( isset($params['password']) && !empty($params['password'])) {
            $validator['password'] = v::key('password', v::notOptional() ->stringType()->length(6, 255)
                ->notBlank() );
        }

        return $validator;

    }

    /**
     * Updates a User.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateAction(Request $request)
    {
        $usersRepository =  $this->getDoctrine()
            ->getRepository(User::class);

        $user = $this->findUserIn($request->attributes->get('user_id'), $usersRepository);

        $this->validateUserId(
            $user,
            $apiToken = $request->headers->get('X-AUTH-TOKEN')
        );

        $params = $request->get('user');
        $params['editedUser'] = $user;

        if ( $validationErrors = $this->getValidationErrorsOn($params, $user, $usersRepository, $params) ) {
            return $this->validationErrorsJson($validationErrors);
        }

        $user->setUsername( $params['username'] );

        if (isset( $params['password'] ) && !empty($params['password'])) {
            $user->setPassword( $params['password'] );
        }

        $usersRepository->persist($user);

        $this->getDoctrine()->getManager()->flush();

        return $this->json([
            'success' => true,
            'data' => $this->getUserJson($user)
        ]);
    }

    /**
     * Finds a product in the ProductsRepository or raises a NotFoundHttpException if absent.
     */
    protected function findUserIn($userId, $usersRepository)
    {
        $user = $usersRepository->find($userId);

        if ($user === null) {
            throw new NotFoundHttpException('The specified user was not found.');
        }

        return $user;
    }

    protected function validateUserId($user, $apiToken)
    {
        $loggedUserId = $this->get('app.api_tokens_repository')->getUserIdAtToken($apiToken);

        if( $user->getId() != $loggedUserId ) {
            throw new UnauthorizedHttpException(null);
        }
    }
}