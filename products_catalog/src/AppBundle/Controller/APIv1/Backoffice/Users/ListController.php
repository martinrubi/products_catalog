<?php

namespace AppBundle\Controller\APIv1\Backoffice\Users;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller
{
    use UsersJsonTrait;

    /**
     * Searches for Users and returns a json with the found users.
     * The search parameter comes as a query parameter and is named 'q'.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */

    public function listAction(Request $request)
    {
        $q = $request->get('q');

        $usersRepository =  $this->getDoctrine()
            ->getRepository(User::class);

        $users = $usersRepository->searchContaining($q);

        return $this->json([
            'success' => true,
            'data' => $this->getUsersJson($users)
        ]);
    }
}