<?php

namespace AppBundle\Controller\APIv1\Backoffice\Products;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Adds the methods to serialize products to JSON associative array.
 */
trait ProductsTrait
{
    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given $products array.
     */
    protected function getProductsJson($products)
    {
        return array_map(
            function($product) {
                return $this->getProductJson($product);
            },
            $products
        );
    }

    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given $product.
     */
    protected function getProductJson($product)
    {
        $category = $product->getCategory();

        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'image' => '/images/products/' . $product->getImage(),
            'category' => [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'icon' => '/images/categories/' . $category->getIcon(),
            ],
        ];
    }

    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given categories.
     */
    protected function getCategoriesJson($categories)
    {
        return array_map(
            function($category) {
                return [
                    'id' => $category->getId(),
                    'name' => $category->getName(),
                    'icon' => '/images/categories/' . $category->getIcon(),
                ];
            },
            $categories
        );
    }

    /**
     * Finds a product in the ProductsRepository or raises a NotFoundHttpException if absent.
     *
     * @param $productId The id of the product to look for.
     * @param $productsRepository The ProductsRepository.
     * @return mixed The Product found.
     */
    protected function findProductIn($productId, $productsRepository)
    {
        $product = $productsRepository->find($productId);

        if ($product === null) {
            throw new NotFoundHttpException('The specified product was not found.');
        }

        return $product;
    }
}