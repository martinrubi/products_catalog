<?php

namespace AppBundle\Controller\APIv1\Backoffice\Products;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Respect\Validation\Validator as v;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CreateController extends Controller
{
    use ProductsTrait;
    use ValidationTrait;

    /**
     * Return the validation for each attribute in the request.
     * @return array
     */
    protected function getValidators($categoriesRepository, $params): array
    {
        v::with('\\AppBundle\\Validations\\');

        $validators = [
            'name' => v::key('name', v::notOptional() ->stringType() ->length(1, 255)
                ->notBlank() ),
            'categoryId' => v::key('categoryId', v::notOptional() ->intVal() ->min(0)
                ->entityExists($categoriesRepository)
            ),
        ];

        if( $params['imageFile'] !== null ) {
            $validators['imageFile'] = v::key('imageFile', v::notOptional() ->file()
                ->image() ->size('1B', '3MB' )
            );
        }

        return $validators;
    }

    /**
     * Creates a product.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function createAction(Request $request)
    {
        $productsRepository =  $this->getDoctrine()
            ->getRepository(Product::class);

        $categoriesRepository =  $this->getDoctrine()
            ->getRepository(Category::class);

        $params = $request->get('product');

        $params['imageFile'] = $request->files->get('productImage');

        if ( $validationErrors = $this->getValidationErrorsOn($params, $categoriesRepository, $params) ) {
            return $this->validationErrorsJson($validationErrors);
        }

        $category = $categoriesRepository->find($params['categoryId']);

        $product = new Product();
        $product->setName( $params['name'] );
        $product->setImageFile( $params['imageFile'] );
        $product->setCategory( $category );

        $productsRepository->persist($product);

        $this->getDoctrine()->getManager()->flush();

        return $this->json([
            'success' => true,
            'data' => $this->getProductJson($product)
        ]);
    }
}