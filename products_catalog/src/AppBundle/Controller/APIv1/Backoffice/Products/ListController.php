<?php

namespace AppBundle\Controller\APIv1\Backoffice\Products;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller
{
    use ProductsTrait;

    /**
     * Searches for products and returns a json with the found products.
     * The search parameter comes as a query parameter and is named 'q'.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listAction(Request $request)
    {
        $q = $request->get('q');

        $productsRepository =  $this->getDoctrine()
            ->getRepository(Product::class);

        $categoriesRepository =  $this->getDoctrine()
            ->getRepository(Category::class);

        $products = $productsRepository->searchContaining($q);
        $categories = $categoriesRepository->findAll();

        $productsJson = $this->getProductsJson($products);
        $categoriesJson = $this->getCategoriesJson($categories);

        return $this->json([
            'success' => true,
            'data' => [
                'products' => $productsJson,
                'categories' => $categoriesJson,
            ]
        ]);
        }
}