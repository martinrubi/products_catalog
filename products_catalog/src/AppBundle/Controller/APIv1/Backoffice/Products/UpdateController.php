<?php

namespace AppBundle\Controller\APIv1\Backoffice\Products;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Validations\EntityExistsValidator;
use Respect\Validation\Validator as v;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UpdateController extends Controller
{
    use ProductsTrait;
    use ValidationTrait;

    /**
     * Return the validation for each attribute in the request.
     * @return array
     */
    protected function getValidators($categoriesRepository, $params): array
    {
        v::with('\\AppBundle\\Validations\\');

        $validators = [
            'name' => v::key('name', v::notOptional() ->stringType() ->length(1, 255)
                ->notBlank() ),
            'categoryId' => v::key('categoryId', v::notOptional() ->intVal() ->min(0)
                ->entityExists($categoriesRepository)
            ),
        ];

        if( $params['imageFile'] !== null ) {
            $validators['imageFile'] = v::key('imageFile', v::notOptional()->file()
                ->image() ->size('1B', '3MB' )
            );
        }

        return $validators;
    }

    /**
     * Updates a product.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateAction(Request $request)
    {
        $productsRepository = $this->getDoctrine()
            ->getRepository(Product::class);

        $categoriesRepository = $this->getDoctrine()
            ->getRepository(Category::class);

        $params = $request->get('product');

        $params['imageFile'] = $request->files->get('productImage');

        if ( $validationErrors = $this->getValidationErrorsOn($params, $categoriesRepository, $params) ) {
            return $this->validationErrorsJson($validationErrors);
        }

        $product = $this->findProductIn($request->attributes->get('product_id'), $productsRepository);

        $category = $categoriesRepository->find($params['categoryId']);

        $product->setName($params['name']);
        $product->setCategory($category);
        if( $params['imageFile'] !== null ) {
            $product->setImage( $params['imageFile']->getFileName() );
            $product->setImageFile( $params['imageFile'] );
        }

        $productsRepository->persist($product);

        $this->getDoctrine()->getManager()->flush();

        return $this->json([
            'success' => true,
            'data' => $this->getProductJson($product)
        ]);
    }
}