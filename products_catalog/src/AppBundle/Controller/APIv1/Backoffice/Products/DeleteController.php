<?php

namespace AppBundle\Controller\APIv1\Backoffice\Products;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Validations\EntityExistsValidator;
use Respect\Validation\Validator as v;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DeleteController extends Controller
{
    use ProductsTrait;

    /**
     * Deletes a product.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $productsRepository = $this->getDoctrine()
            ->getRepository(Product::class);

        $product = $this->findProductIn($request->attributes->get('product_id'), $productsRepository);

        $productsRepository->remove($product);

        $this->getDoctrine()->getManager()->flush();

        return $this->json([
            'success' => true,
            'data' => $this->getProductJson($product)
        ]);
    }
}