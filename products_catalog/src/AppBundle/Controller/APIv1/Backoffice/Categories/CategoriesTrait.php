<?php

namespace AppBundle\Controller\APIv1\Backoffice\Categories;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Adds the methods to serialize categories to JSON associative array.
 */
trait CategoriesTrait
{
    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given $categories array.
     */
    protected function getCategoriesJson($categories)
    {
        return array_map(
            function($category) {
                return $this->getCategoryJson($category);
            },
            $categories
        );
    }

    /**
     * Returns an associative array representing the JSON that will be returned in the response for the
     * given $category.
     */
    protected function getCategoryJson($category)
    {
        return [
            'id' => $category->getId(),
            'name' => $category->getName(),
            'icon' => '/images/categories/' . $category->getIcon(),
        ];
    }

    protected function findCategoryIn($categoryId, $categoriesRepository)
    {
        $category = $categoriesRepository->find($categoryId);

        if ($category === null) {
            throw new NotFoundHttpException('The specified category was not found.');
        }

        return $category;
    }
}