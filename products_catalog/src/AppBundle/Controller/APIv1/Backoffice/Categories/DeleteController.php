<?php

namespace AppBundle\Controller\APIv1\Backoffice\Categories;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\Category;
use Respect\Validation\Validator as v;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DeleteController extends Controller
{
    use CategoriesTrait;
    use ValidationTrait;

    /**
     * Return the validation for each attribute in the request.
     * @return array
     */
    protected function getValidators($em)
    {
        v::with('\\AppBundle\\Validations\\');

        $validators = [
            'name' => v::hasNoProducts($em),
        ];

        return $validators;
    }

    /**
     * Updates a Category.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $categoriesRepository =  $this->getDoctrine()
            ->getRepository(Category::class);

        $category = $this->findCategoryIn(
            $request->attributes->get('category_id'),
            $categoriesRepository
        );

        if ( $validationErrors = $this->getValidationErrorsOn($category, $this->getDoctrine()->getManager()) ) {
            return $this->validationErrorsJson($validationErrors);
        }

        $categoriesRepository->remove($category);

        $this->getDoctrine()->getManager()->flush();

        return $this->json(
            [
                'success' => true,
                'data' => $this->getCategoryJson($category)
            ]
        );
    }
}