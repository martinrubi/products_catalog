<?php

namespace AppBundle\Controller\APIv1\Backoffice\Categories;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller
{
    use CategoriesTrait;

    /**
     * Searches for categories and returns a json with the found categories.
     * The search parameter comes as a query parameter and is named 'q'.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */

    public function listAction(Request $request)
    {
        $q = $request->get('q');

        $categoriesRepository =  $this->getDoctrine()
            ->getRepository(Category::class);

        $categories = $categoriesRepository->searchContaining($q);

        return $this->json(
            [
                'success' => true,
                'data' => $this->getCategoriesJson($categories)
            ]
        );
    }
}