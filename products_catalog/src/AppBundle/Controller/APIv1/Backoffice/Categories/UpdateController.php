<?php

namespace AppBundle\Controller\APIv1\Backoffice\Categories;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\Category;
use Respect\Validation\Validator as v;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UpdateController extends Controller
{
    use CategoriesTrait;
    use ValidationTrait;

    /**
     * Return the validation for each attribute in the request.
     * @return array
     */
    protected function getValidators($category, $categoriesRepository, $params): array
    {
        v::with('\\AppBundle\\Validations\\');

        $validators = [
            'name' => v::key('name', v::notOptional() ->stringType() ->length(1, 255)
                ->notBlank() ->doesNotExist($categoriesRepository, 'name', $category) ),
        ];

        if ($params['iconFile'] !== null) {
            $validators['iconFile'] = v::key('iconFile', v::notOptional() ->file()
                ->image() ->size('1B', '3MB' ) );
        }

        return $validators;
    }

    /**
     * Updates a Category.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateAction(Request $request)
    {
        $categoriesRepository =  $this->getDoctrine()
            ->getRepository(Category::class);

        $category = $this->findCategoryIn(
            $request->attributes->get('category_id'),
            $categoriesRepository
        );

        $params = $request->get('category');

        $params['iconFile'] = $request->files->get('categoryIcon');

        if ( $validationErrors = $this->getValidationErrorsOn($params, $category, $categoriesRepository, $params) ) {
            return $this->validationErrorsJson($validationErrors);
        }

        $category->setName( $params['name'] );
        if ($params['iconFile'] !== null) {
            $category->setIcon($params['iconFile']->getFileName());
            $category->setIconFile( $params['iconFile'] );
        }

        $categoriesRepository->persist($category);

        $this->getDoctrine()->getManager()->flush();

        return $this->json(
            [
                'success' => true,
                'data' => $this->getCategoryJson($category)
            ]
        );
    }
}