<?php

namespace AppBundle\Controller\APIv1\Backoffice\Categories;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Respect\Validation\Validator as v;

class CreateController extends Controller
{
    use CategoriesTrait;
    use ValidationTrait;

    /**
     * Return the validation for each attribute in the request.
     * @return array
     */
    protected function getValidators($categoriesRepository): array
    {
        v::with('\\AppBundle\\Validations\\');

        return [
            'name' => v::key('name', v::notOptional() ->stringType() ->length(1, 255)
                ->notBlank() ->doesNotExist($categoriesRepository, 'name') ),
            'iconFile' => v::key('iconFile', v::notOptional() ->file()
                ->image() ->size('1B', '3MB' ) ),
        ];
    }

    /**
     * Creates a Category.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function createAction(Request $request)
    {
        $categoriesRepository =  $this->getDoctrine()
            ->getRepository(Category::class);

        $params = $request->get('category');

        $params['iconFile'] = $request->files->get('categoryIcon');

        if ( $validationErrors = $this->getValidationErrorsOn($params, $categoriesRepository) ) {
            return $this->validationErrorsJson($validationErrors);
        }

        $category = new Category();
        $category->setName( $params['name'] );
        $category->setIconFile( $params['iconFile'] );

        $categoriesRepository->persist($category);

        $this->getDoctrine()->getManager()->flush();

        return $this->json(
            [
                'success' => true,
                'data' => $this->getCategoryJson($category)
            ]
        );
    }
}