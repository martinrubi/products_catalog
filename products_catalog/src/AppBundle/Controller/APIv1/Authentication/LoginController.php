<?php

namespace AppBundle\Controller\APIv1\Authentication;

use AppBundle\Controller\APIv1\ValidationTrait;
use AppBundle\Entity\User;
use Respect\Validation\Validator as v;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handles the login of the application.
 * For the login compares the given credentials with the user credentials.
 * If they match generates an apiToken, stores it in Redis and sends the apiToken to the client.
 * From there on the client will be authorized to that apiToken.
 */
class LoginController extends Controller
{
    use ValidationTrait;

    /**
     * Return the validation for each attribute in the request.
     * @return array
     */
    protected function getValidators(): array
    {
        return [
            'username' => v::key('_username', v::notOptional()->stringType()->length(1, 255)->notBlank() ),
            'password' => v::key('_password', v::notOptional()->stringType()->length(1, 255)->notBlank() ),
        ];
    }

    public function loginAction(Request $request)
    {
        if ( $validationErrors = $this->getValidationErrorsOn($request->request->all()) ) {
            return $this->validationErrorsJson($validationErrors);
        }

        $username = $request->get('_username');
        $password = $request->get('_password');

        $usersRepository =  $this->getDoctrine()
            ->getRepository(User::class);

        $user = $usersRepository->findByUsernameOrEmail($username);

        if ($user === null || ! $user->isValidPassword($password) ) {
            return $this->onLoginFailed();

        }

        $apiTokensRepository =  $this->get('app.api_tokens_repository');

        $apiToken = $apiTokensRepository->createTokenFor($user->getId());

        return $this->json([
            'success' => true,
            'data' => [
                'token' => $apiToken,
                'id' => $user->getId(),
                'username' => $user->getUsername(),
            ]
        ]);
    }

    protected function onLoginFailed()
    {
        return $this->json([
            'success' => false,
            'errors' => [
                'username' => 'Invalid user or password.',
                'password' => 'Invalid user or password.',
            ]
        ]);
    }
}