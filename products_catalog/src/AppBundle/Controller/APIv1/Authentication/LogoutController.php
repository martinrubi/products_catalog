<?php

namespace AppBundle\Controller\APIv1\Authentication;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Handles the logout of the application.
 * Just drop the apiToken.
 */
class LogoutController extends Controller
{
    public function logoutAction(Request $request)
    {
        $apiToken = $request->headers->get('X-AUTH-TOKEN');

        if ($apiToken === null ) {
            return $this->onLogoutFailed();

        }

        $this->get('app.api_tokens_repository')->removeToken($apiToken);

        return $this->json([
            'success' => true,
            'data' => []
        ]);
    }

    protected function onLogoutFailed()
    {
        return $this->json([
            'success' => false,
            'errors' => []
        ]);
    }
}