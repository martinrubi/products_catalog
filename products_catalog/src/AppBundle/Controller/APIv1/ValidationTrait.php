<?php

namespace AppBundle\Controller\APIv1;

use Respect\Validation\Exceptions\ValidationException;

/**
 * Adds methods to validate attributes.
 * The class including this Trait is expected to have a method named
 *
 *          protected function getValidators() : array
 *
 * returning an array of validators, on for each attribute.
 */
trait ValidationTrait
{
    protected function getValidationErrorsOn($params, ...$additionalArguments)
    {
        $validationErrors = [];

        foreach ($this->getValidators(...$additionalArguments) as $attribute => $validator) {
            try {
                $validator->check($params);
            } catch(ValidationException $exception) {
                $validationErrors[$attribute] = $exception->getMainMessage();
            }
        }

        return $validationErrors;
    }

    protected function validationErrorsJson($validationErrors)
    {
        return $this->json([
            'success' => false,
            'errors' => $validationErrors
        ]);
    }
}