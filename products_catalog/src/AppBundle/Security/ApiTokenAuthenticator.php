<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Doctrine\ORM\EntityManager;

/**
 * Handles the access to the API endpoints that require authorization.
 * If the request provides a valid apiToken for a user and the user exists then authorize it, otherwise don't.
 */
class ApiTokenAuthenticator extends AbstractGuardAuthenticator
{
    public function __construct(EntityManager $em, ApiTokensRepository $apiTokensRepository)
    {
        $this->em = $em;
        $this->apiTokensRepository = $apiTokensRepository;
    }

    /**
     * Called on every request. Return the apiToken given in the request.
     */
    public function getCredentials(Request $request)
    {
        return [
            'apiToken' => $request->headers->get('X-AUTH-TOKEN'),
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $apiToken = $credentials['apiToken'];

        if (null === $apiToken) {
            return;
        }

        $userId = $this->apiTokensRepository->getUserIdAtToken($apiToken);

        if ($userId === null ) {
            return;
        }

        return $this->em->getRepository(User::class)
            ->find($userId);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case

        // return true to cause authentication success
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'success' => false,
            'errors' => ['Invalid credentials.']
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            'success' => false,
            'errors' => ['Authentication required.']
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}