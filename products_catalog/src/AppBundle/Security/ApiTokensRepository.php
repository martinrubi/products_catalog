<?php

namespace AppBundle\Security;

use Symfony\Component\Cache\Adapter\AdapterInterface;

/**
 * Administrates the session api tokens.
 */
class ApiTokensRepository
{
    public function __construct(AdapterInterface $cache)
    {
        $this->cache = $cache;
    }

    public function createTokenFor($userId)
    {
        $apiToken = $this->generateNewToken();

        $item = $this->cache->getItem('api_token.' . $apiToken);

        $item->expiresAfter(3600);

        $item->set($userId);

        $this->cache->save($item);

        return $apiToken;
    }

    public function getUserIdAtToken($apiToken)
    {
        $cachedItem = $this->cache->getItem('api_token.' . $apiToken);

        if( $cachedItem->isHit() === false ) {
            return null;
        }

        return $cachedItem->get();
    }

    public function removeToken($apiToken)
    {
        $this->cache->deleteItem('api_token.' . $apiToken);
    }

    protected function generateNewToken()
    {
        return bin2hex( random_bytes(15) );
    }
}