<?php

namespace AppBundle\Entity;

/**
 * Category
 */
class Category
{
    public function __construct(string $name = null, string $icon = null)
    {
        $this->name = $name;
        $this->icon = $icon;
    }

    private $id;

    private $name;

    private $icon;

    // Not persisted, required by uploader
    private $iconFile;


    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIconFile($iconFile = null)
    {
        $this->iconFile = $iconFile;
    }

    public function getIconFile()
    {
        return $this->iconFile;
    }
}

