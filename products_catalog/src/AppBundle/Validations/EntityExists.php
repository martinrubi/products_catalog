<?php

namespace AppBundle\Validations;

use Doctrine\ORM\EntityRepository;
use Respect\Validation\Rules\AbstractRule;

class EntityExists extends AbstractRule
{
    public function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function validate($entityId)
    {
        $entity = $this->repository->find($entityId);

        return $entity !== null;
    }
}