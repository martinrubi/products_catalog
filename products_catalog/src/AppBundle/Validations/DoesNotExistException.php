<?php

namespace AppBundle\Validations;

use Respect\Validation\Exceptions\ValidationException;

class DoesNotExistException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'The {{name}} already exists.',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => 'The {{name}} should exist.',
        ]
    ];
}