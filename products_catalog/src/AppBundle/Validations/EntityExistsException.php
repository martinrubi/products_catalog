<?php

namespace AppBundle\Validations;

use Respect\Validation\Exceptions\ValidationException;

class EntityExistsException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'The {{name}} was not found.',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => 'The {{name}} should not exist.',
        ]
    ];
}