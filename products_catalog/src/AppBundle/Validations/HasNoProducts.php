<?php

namespace AppBundle\Validations;

use AppBundle\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Respect\Validation\Rules\AbstractRule;

class HasNoProducts extends AbstractRule
{
    public function __construct(EntityManager $categoriesRepository)
    {
        $this->em = $categoriesRepository;
    }

    public function validate($category)
    {
        $count = $this->em
            ->createQuery(
                'SELECT count(p) FROM AppBundle:Product p JOIN p.category c WHERE c.id LIKE :id'
            )
            ->setParameter('id', $category->getId())
            ->getSingleScalarResult();

        return (int) $count === 0;
    }
}