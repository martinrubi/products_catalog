<?php

namespace AppBundle\Validations;

use Respect\Validation\Exceptions\ValidationException;

class HasNoProductsException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'The category has products.',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => 'The category has no products.',
        ]
    ];
}