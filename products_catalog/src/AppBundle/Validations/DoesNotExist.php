<?php

namespace AppBundle\Validations;

use Doctrine\ORM\EntityRepository;
use Respect\Validation\Rules\AbstractRule;

class DoesNotExist extends AbstractRule
{
    public function __construct(EntityRepository $repository, $field, $entity=null)
    {
        $this->repository = $repository;
        $this->field = $field;
        $this->entity = $entity;
    }

    public function validate($value)
    {
        if( $this->entity === null ) {

            $found = $this->repository->createQueryBuilder('e')
                ->select('count(e.id)')
                ->where("e.{$this->field} = :value")
                ->setParameters([
                    'value' => $value
                ])
                ->getQuery()
                ->getSingleScalarResult();

        } else {

            $found = $this->repository->createQueryBuilder('e')
                ->select('count(e.id)')
                ->where("e.id <> :id AND e.{$this->field} = :value")
                ->setParameters([
                    'id' => $this->entity->getId(),
                    'value' => $value
                ])
                ->getQuery()
                ->getSingleScalarResult();

        }

        return (int) $found === 0;
    }
}