require('./materialize_init.js')
require('../css/app.scss')

import ReactDOM from 'react-dom'
import React from 'react'
import {BrowserRouter, Route, Switch} from "react-router-dom"

import Frontend from './frontend/Frontend'
import Login from "./routing/Login"
import Backoffice from "./backoffice/Backoffice"
import Authentication from "./models/Authentication"
import AuthorizedRoute from "./routing/AuthorizedRoute"

class App extends React.Component {
    constructor(props) {
        super(props)

        Authentication.onUnauthenticatedAccess = this.onUnauthenticatedAccess.bind(this)
    }

    onUnauthenticatedAccess() {
        this.forceUpdate()
    }

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <AuthorizedRoute path="/admin" component={Backoffice} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/" component={Frontend} />
                </Switch>
            </BrowserRouter>
         );
    }
}

ReactDOM.render(
    <App name="there" />,
    document.getElementById('root')
);