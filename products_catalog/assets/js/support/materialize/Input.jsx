import React, { Component } from 'react'
import If from '../conditionals/If'

class Input extends React.Component {
    render() {
        const col = this.props.col ? 'col ' + this.props.col : undefined
        const id = this.props.id
        const name = this.props.name
        const type = this.props.type
        const className = this.props.className ? this.props.className : ''
        const value = this.props.value
        const onChange = this.props.onChange
        const placeholder = this.props.placeholder
        const label = this.props.label
        const validationError = this.props.validationError
        const leftIcon = this.props.leftIcon

        const validationClass = validationError === undefined ?
            '' : (validationError === '' ? 'valid' : 'invalid')

        return (
            <div className={col}>
                <div className="input-field">
                    <If condition={leftIcon}
                        render={
                            () => <i className="material-icons prefix">{leftIcon}</i>
                        }
                    />
                    <input
                        id={id}
                        type={type}
                        name={name}
                        className={['validate', validationClass, className].join(' ')}
                        value={value}
                        onChange={onChange}
                        placeholder={placeholder}
                    />
                    <If condition={label}
                        render={
                            () => <label htmlFor={name}>{label}</label>
                        }
                    />
                    <If condition={validationError}
                        render={
                            () => <span className="helper-text" data-error={validationError} />
                        }
                    />
                </div>
            </div>
        )
    }
}

export default Input