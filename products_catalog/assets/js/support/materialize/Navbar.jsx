import React, { Component } from 'react'

class Navbar extends React.Component {

    renderLinks(links) {
        return this.props.links.map((link, index)=>{
            return (<li key={index}>
                {link}
            </li>)
        });
    }

    render() {
        const title = this.props.title
        const links = this.renderLinks(this.props.links)

        return (
            <nav>
                <div className="nav-wrapper">
                    <a href="#" className="brand-logo center">{title}</a>
                    <ul id="navbar" className="right hide-on-med-and-down">
                        {links}
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navbar