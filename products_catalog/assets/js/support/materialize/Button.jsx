import React, { Component } from 'react'
import If from '../conditionals/If'

class Button extends React.Component {

    render() {
        const label = this.props.label
        const className = this.props.className ? this.props.className : ''
        const disabled = this.props.disabled === true ? ' disabled' : ''
        const onClick = this.props.onClick
        const icon = this.props.icon

        return (
            <a
                className={['waves-effect waves-light', className, disabled].join(' ')}
                onClick={onClick}
            >
                <If condition={icon} render={
                    () => <i className="material-icons right">{icon}</i>
                }/>
                {label}
            </a>
        )
    }
}

export default Button