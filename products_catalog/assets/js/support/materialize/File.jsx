import React, { Component } from 'react'
import If from '../conditionals/If'

class File extends React.Component {

    render() {
        const col = this.props.col
        const id = this.props.id
        const name = this.props.name
        const className = this.props.className ? this.props.className : ''
        const onChange = this.props.onChange
        const placeholder = this.props.placeholder
        const label = this.props.label
        const validationError = this.props.validationError
        const leftIcon = this.props.leftIcon

        const validationClass = validationError === undefined ?
            '' : (validationError === '' ? 'valid' : 'invalid')

        return (
            <div className={'col ' + col }>
                <div className="file-field input-field">
                    <If condition={leftIcon}
                        render={
                            () => <i className="material-icons prefix">{leftIcon}</i>
                        }
                    />
                    <div className="btn-small">
                        <span>{label}</span>
                        <input
                            id={id}
                            type="file"
                            onChange={onChange}
                        />
                    </div>
                    <div className="file-path-wrapper">
                        <input
                            className={['file-path validate', validationClass, className].join(' ')}
                            type="text"
                            placeholder={placeholder}
                        />
                        <If condition={validationError}
                            render={
                                () => <span className="helper-text" data-error={validationError} />
                            }
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default File