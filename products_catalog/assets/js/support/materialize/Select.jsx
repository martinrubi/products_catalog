import React, { Component } from 'react'
import If from '../conditionals/If'

class Select extends React.Component {

    renderOptions() {
        const options = Object.assign([], this.props.options)

        options.unshift(this.props.nullOption)

        return options.map( (option) => {
            const value = this.props.getOptionValue(option)
            const text = this.props.getOptionText(option)
            const image = this.props.getOptionImage(option)
            const className = this.props.getOptionClassName(option)

            return (
                <option key={value}
                        value={value}
                        data-icon={image}
                        className={className}
                >
                    {text}
                </option>
            )
        })
    }

    render() {
        const col = this.props.col
        const id = this.props.id
        const className = this.props.className ? this.props.className : ''
        const name = this.props.name
        const value = this.props.value
        const onChange = this.props.onChange
        const label = this.props.label
        const options = this.props.options
        const validationError = this.props.validationError

        const validationClass = validationError === undefined ?
            '' : (validationError === '' ? 'valid' : 'invalid')

        return (
            <div className={'col ' + col} >
                <div className="input-field">
                    <select value={value}
                            id={id}
                            className={['validate', validationClass, className].join(' ')}
                            name={name}
                            onChange={onChange}
                    >
                        {this.renderOptions()}
                    </select>
                    <If
                        condition={label}
                        render={
                            () => <label htmlFor={name}>{label}</label>
                        }
                    />
                    <If condition={validationError}
                        render={
                            () => (
                                <span className="helper-text" data-error={validationError}>
                                    {validationError}
                                </span>
                            )
                        }
                    />
                </div>
            </div>
        )
    }
}

export default Select