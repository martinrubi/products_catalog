import React, { Component } from 'react'

class SubmitButton extends React.Component {

    render() {
        const col = this.props.col
        const label = this.props.label
        const disabled = this.props.disabled === true ? ' disabled' : ''

        return (
            <div className={'col ' + col}>
                <button
                    className={'btn btn-small waves-effect waves-light' + disabled}
                    type="submit"
                    name="action"
                >{label}<i className="material-icons right">send</i>
                </button>
            </div>
        )
    }
}

export default SubmitButton