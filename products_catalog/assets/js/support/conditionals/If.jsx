import React, { Component } from 'react'

/**
 * A conditional component that renders an expression depending on the value of a logical expression.
 * The props props.render and props.else are closure instead of expressions to avoid evaluating the
 * expression in case the condition is not met.
 *
 * Usage:
 *
 *      <If
 *          condition={value===true}
 *          render={
 *               () => <div>...<div/>
 *           }
 *      />
 *
 *      <If
 *          condition={value===true}
 *          render={
 *               () => <div>...<div/>
 *           }
 *          else={
 *               () => <div>...<div/>
 *          }
 *      />
 */
class If extends React.Component {

    render() {
        if( this.props.condition ) {
            return this.props.render()
        }

        return this.props.else ?  this.props.else() : null
    }
}

export default If