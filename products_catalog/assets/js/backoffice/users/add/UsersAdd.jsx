import React, { Component } from 'react'
import Users from '../../../models/backoffice/Users'
import Input from '../../../support/materialize/Input'
import SubmitButton from '../../../support/materialize/SubmitButton'

class UsersAdd extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            email: '',
            password: '',
            validationErrors: undefined,
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUsernameChanged = this.handleUsernameChanged.bind(this)
        this.handleEmailChanged = this.handleEmailChanged.bind(this)
        this.handlePasswordChanged = this.handlePasswordChanged.bind(this)
    }

    initializeFormLabels() {
        M.updateTextFields()
    }

    notifySuccessfullCreate() {
        M.toast({html: 'The user was created!'})
    }

    onSubmitResponse(response) {
        if (response.success) {
            this.notifySuccessfullCreate()

            this.setState({
                username: '',
                email: '',
                password: '',
                validationErrors: undefined,
            })
        } else {
            this.setState({validationErrors: undefined})
            this.setState({validationErrors: response.errors})
        }
    }

    handleSubmit(event) {
        event.preventDefault()

        this.createQuery = Users.create({
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
        }).whenDataArrives( this.onSubmitResponse.bind(this));
    }

    handleUsernameChanged(event) {
        this.setState({
            username: event.target.value,
        })
    }

    handleEmailChanged(event) {
        this.setState({
            email: event.target.value,
        })
    }

    handlePasswordChanged(event) {
        this.setState({
            password: event.target.value,
        })
    }

    componentDidMount() {
        this.initializeFormLabels()
    }

    componentWillUnmount() {
        if (this.createQuery) {
            this.createQuery.abort()
        }
    }

    render() {
        const validationErrors = this.state.validationErrors

        return (
            <div className="row">
                <form className="col s8 offset-s1"
                      onSubmit={this.handleSubmit}
                >
                    <Input
                        col="s12"
                        type="text"
                        value={this.state.username}
                        onChange={this.handleUsernameChanged}
                        placeholder="Username ..."
                        label="Username"
                        validationError={validationErrors ? (validationErrors.username ? validationErrors.username : '') : undefined}

                    />
                    <Input
                        col="s12"
                        type="text"
                        value={this.state.email}
                        onChange={this.handleEmailChanged}
                        placeholder="Email ..."
                        label="Email"
                        validationError={validationErrors ? (validationErrors.email ? validationErrors.email : '') : undefined}
                    />
                    <Input
                        col="s12"
                        type="text"
                        value={this.state.password}
                        onChange={this.handlePasswordChanged}
                        placeholder="Password ..."
                        label="Password"
                        validationError={validationErrors ? (validationErrors.password ? validationErrors.password : '') : undefined}
                    />
                    <SubmitButton
                        col="s3 offset-s10"
                        label="Add"
                    />
                </form>
            </div>
        );
    }
}

export default UsersAdd;