import React, { Component } from 'react'
import Authentication from '../../../models/Authentication'
import Users from '../../../models/backoffice/Users'
import Input from '../../../support/materialize/Input'
import SubmitButton from '../../../support/materialize/SubmitButton'

class EditUser extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            username: Authentication.getUser().username,
            password: '',
            validationErrors: undefined,
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUsernameChanged = this.handleUsernameChanged.bind(this)
        this.handlePasswordChanged = this.handlePasswordChanged.bind(this)
    }

    initializeFormLabels() {
        M.updateTextFields()
    }

    notifySuccessfullUpdate() {
        M.toast({html: 'The user information was updated!'})
    }

    onSubmitResponse(response) {
        if (response.success) {
            this.notifySuccessfullUpdate()

            const $user = Authentication.getUser()
            $user.username = response.data.username
            Authentication.storeUser($user)

            this.setState({
                validationErrors: undefined,
            })
        } else {
            this.setState({validationErrors: undefined})
            this.setState({validationErrors: response.errors})
        }
    }

    handleSubmit(event) {
        event.preventDefault()

        this.updateQuery = Users.update({
            username: this.state.username,
            password: this.state.password,
        }).whenDataArrives( this.onSubmitResponse.bind(this))
    }

    handleUsernameChanged(event) {
        this.setState({
            username: event.target.value,
        })
    }

    handlePasswordChanged(event) {
        this.setState({
            password: event.target.value,
        })
    }

    componentDidMount() {
        this.initializeFormLabels()
    }

    componentWillUnmount() {
        if (this.updateQuery) {
            this.updateQuery.abort()
        }
    }

    render() {
        const validationErrors = this.state.validationErrors

        return (
            <div className="row">
                <form className="col s8 offset-s1"
                      onSubmit={this.handleSubmit}
                >
                    <Input
                        col="s12"
                        type="text"
                        value={this.state.username}
                        onChange={this.handleUsernameChanged}
                        placeholder="Username ..."
                        label="Username"
                        validationError={validationErrors ? (validationErrors.username ? validationErrors.username : '') : undefined}

                    />
                    <Input
                        col="s12"
                        type="password"
                        value={this.state.password}
                        onChange={this.handlePasswordChanged}
                        placeholder="Password ..."
                        label="Password (optional)"
                        validationError={validationErrors ? (validationErrors.password ? validationErrors.password : '') : undefined}

                    />
                    <SubmitButton
                        col="s3 offset-s10"
                        label="Update"
                    />
                </form>
            </div>
        );
    }
}

export default EditUser;