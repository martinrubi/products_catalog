import React, { Component } from 'react'
import Users from '../../../models/backoffice/Users'

class UsersList extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            users: [],
        }
    }

    requestUsers() {
        this.usersQuery = Users.searchFor('')
            .whenDataArrives( (response) => {
                this.setState({
                    users: response.data
                })
            })
    }

    componentWillMount() {
        this.requestUsers()
    }

    componentWillUnmount() {
        if (this.usersQuery) {
            this.usersQuery.abort()
        }
    }

    renderUserRows() {
        const users = this.state.users

        return users.map( (user) => {
            return (
                <tr key={user.id}>
                    <td>{user.id}</td>
                    <td>{user.username}</td>
                    <td>{user.email}</td>
                </tr>
            )
        })
    }

    render() {
        const userRows = this.renderUserRows()

        return (
            <div className="row">
                <table className="col s9 offset-s1">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Username</th>
                        <th>Email</th>
                    </tr>
                    </thead>

                    <tbody>
                    {userRows}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default UsersList