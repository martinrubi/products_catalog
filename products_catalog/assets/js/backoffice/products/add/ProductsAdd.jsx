import React, { Component } from 'react'
import Categories from "../../../models/backoffice/Categories"
import Products from "../../../models/backoffice/Products";
import Input from '../../../support/materialize/Input'
import Select from '../../../support/materialize/Select'
import File from '../../../support/materialize/File'
import SubmitButton from '../../../support/materialize/SubmitButton'

class ProductsAdd extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            image: '',
            imageFile: undefined,
            categoryId: '',
            categories: [],
            validationErrors: undefined,
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleNameChanged = this.handleNameChanged.bind(this)
        this.handleImageChanged = this.handleImageChanged.bind(this)
        this.handleCategoryChanged = this.handleCategoryChanged.bind(this)
    }

    initializeFormLabels() {
        M.updateTextFields()
    }

    initializeDropDownSelect() {
        const elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {});
    }

    notifySuccessfullCreate() {
        M.toast({html: 'The product was created!'})
    }

    requestCategories() {
        this.categoriesQuery = Categories.searchFor('')
            .whenDataArrives( (response) => {
                this.setState({
                    categories: response.data
                })

                this.initializeDropDownSelect()
            })
    }

    onSubmitResponse(response) {
        if (response.success) {
            this.notifySuccessfullCreate()

            this.setState({
                name: '',
                image: '',
                validationErrors: undefined,
            })
        } else {
            this.setState({validationErrors: undefined})
            this.setState({validationErrors: response.errors})
        }
    }

    handleSubmit(event) {
        event.preventDefault()

        this.createQuery = Products.create({
            name: this.state.name,
            imageFile: this.state.imageFile,
            categoryId: this.state.categoryId,
        }).whenDataArrives( this.onSubmitResponse.bind(this));
    }

    handleNameChanged(event) {
        this.setState({
            name: event.target.value,
        })
    }

    handleImageChanged(event) {
        const imageFile = document.querySelector('#imageFile').files[0];

        this.setState({
            imageFile: imageFile,
        })
    }

    handleCategoryChanged(event) {
        this.setState({
            categoryId: event.target.value,
        })
    }

    componentDidMount() {
        this.requestCategories()
        this.initializeFormLabels()
    }

    componentWillUnmount() {
        if (this.categoriesQuery) {
            this.categoriesQuery.abort()
        }
    }

    render() {
        const validationErrors = this.state.validationErrors

        return (
            <div className="row">
                <form className="col s8 offset-s1"
                      onSubmit={this.handleSubmit}
                >
                    <Input
                        col="s12"
                        type="text"
                        value={this.state.name}
                        onChange={this.handleNameChanged}
                        placeholder="Name ..."
                        label="Name"
                        validationError={validationErrors ? (validationErrors.name ? validationErrors.name : '') : undefined}
                    />
                    <Select
                        col="s12"
                        className="icons"
                        value={this.state.categoryId}
                        onChange={this.handleCategoryChanged}
                        options={this.state.categories}
                        nullOption={ {id:'', name: 'Choose a Category', icon: ''} }
                        getOptionValue={(category)=>{return category.id}}
                        getOptionText={(category)=>{return category.name}}
                        getOptionImage={(category)=>{return category.icon}}
                        getOptionClassName={(category)=>{return "left circle"}}
                        label="Category"
                        validationError={validationErrors ? (validationErrors.categoryId ? validationErrors.categoryId : '') : undefined}
                    />
                    <File
                        id="imageFile"
                        col="s12"
                        onChange={this.handleImageChanged}
                        placeholder="Choose an image ..."
                        label="Image"
                        validationError={validationErrors ? (validationErrors.imageFile ? validationErrors.imageFile : '') : undefined}
                    />
                    <SubmitButton
                        col="s3 offset-s10"
                        label="Add"
                    />
                </form>
            </div>
        );
    }
}

export default ProductsAdd;