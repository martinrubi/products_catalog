import React, { Component } from 'react'

class ProductCard extends React.Component {
    render() {
        const productName = this.props.name;
        const productImage = this.props.image;
        const categoryName = this.props.categoryName
        const categoryIcon = this.props.categoryIcon

        return (
            <div className="col s12">
                <div className="card horizontal">
                    <div className="card-image">
                        <img src={productImage} />
                    </div>
                    <div className="card-stacked">
                        <div className="card-content">
                            <div className="row">
                                <div className="col s12">
                                    <span className="card-title grey-text text-darken-4">{productName}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s4">
                                    <img className="categoryIcon" src={categoryIcon} />
                                </div>
                                <div className="col s12 valign-wrapper">
                                    {categoryName}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductCard;