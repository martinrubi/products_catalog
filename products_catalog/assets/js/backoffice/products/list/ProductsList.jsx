import React, { Component } from 'react'
import Products from '../../../models/backoffice/Products'
import ProductEditBox from "./ProductEditBox"

class ProductsList extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            products: [],
            categories: [],
        }

        this.handleProductDeleted = this.handleProductDeleted.bind(this)
    }

    notifySuccessfullDelete() {
        M.toast({html: 'The product was deleted!'})
    }

    requestProducts() {
        this.productsQuery = Products.searchFor('')
            .whenDataArrives( (response) => {
                this.setState({
                    products: response.data.products,
                    categories: response.data.categories
                })
            })
    }

    componentWillMount() {
        this.requestProducts()
    }

    componentWillUnmount() {
        if (this.productsQuery) {
            this.productsQuery.abort()
        }
    }

    handleProductDeleted(deletedProduct) {
        const productsWithRemovedItem = this.state.products.filter((product) => {
            return product.id != deletedProduct.id
        })

        this.setState({
            products: productsWithRemovedItem,
        })

        this.notifySuccessfullDelete()
    }

    render() {
        const products = this.state.products;
        const categories = this.state.categories

        return products.map( (product) => {
            return (
                <ProductEditBox key={product.id}
                    id={product.id}
                    name={product.name}
                    image={product.image}
                    categoryId={product.category.id}
                    categoryName={product.category.name}
                    categoryIcon={product.category.icon}
                    categories={categories}
                    handleProductDeleted={this.handleProductDeleted}
                />
            )
        })
    }
}

export default ProductsList