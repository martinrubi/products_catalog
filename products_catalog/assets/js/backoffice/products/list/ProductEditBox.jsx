import React, { Component } from 'react'
import ProductCard from "./ProductCard"
import ProductForm from "./ProductForm"
import Products from "../../../models/backoffice/Products"

class ProductEditBox extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            id: props.id,
            name: props.name,
            image: props.image,
            imageFile: undefined,
            categoryId: props.categoryId,
            categoryName: props.categoryName,
            categoryIcon: props.categoryIcon,
            modified: false,
            validationErrors: undefined,
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleNameChanged = this.handleNameChanged.bind(this)
        this.handleImageChanged = this.handleImageChanged.bind(this)
        this.handleCategoryChanged = this.handleCategoryChanged.bind(this)
        this.handleDeleteClicked = this.handleDeleteClicked.bind(this)
    }

    notifySuccessfullUpdate() {
        M.toast({html: 'The product was updated!'})
    }

    onSubmitResponse(response) {
        if (response.success) {
            this.notifySuccessfullUpdate()

            this.setState({validationErrors: []})
            this.setState({
                image: response.data.image,
                modified: false,
                validationErrors: undefined,
            })
        } else {
            this.setState({validationErrors: undefined})
            this.setState({validationErrors: response.errors})
        }
    }

    onProductDeleted(response) {
        if (response.success) {
            this.props.handleProductDeleted({
                id: this.props.id
            })
        } else {
            this.setState({validationErrors: undefined})
            this.setState({validationErrors: response.errors})
        }
    }

    handleSubmit(event) {
        event.preventDefault()

        this.updateQuery = Products.update(
            {
                id: this.state.id,
                name: this.state.name,
                imageFile: this.state.imageFile,
                categoryId: this.state.categoryId
            }
        ).whenDataArrives(this.onSubmitResponse.bind(this));
    }

    handleDeleteClicked() {
        this.deleteQuery = Products.remove({
            id: this.state.id
        }).whenDataArrives(this.onProductDeleted.bind(this));
    }

    handleNameChanged(event) {
        this.setState({
            name: event.target.value,
            modified: true,
        })
    }

    handleImageChanged(event) {
        const inputSelector = '#imageFile-' + this.state.id
        const imageFile = document.querySelector(inputSelector).files[0];

        this.setState({
            imageFile: imageFile,
            modified: true,
        })
    }

    handleCategoryChanged(event) {
        const categoryId = event.target.value

        let category = this.props.categories.find((category) => {
            return category.id == categoryId
        })

        if (category === undefined) {
            category = {}
        }

        this.setState({
            categoryId: category.id,
            categoryName: category.name,
            categoryIcon: category.icon,
            modified: true,
        })
    }

    componentWillUnmount() {
        if (this.updateQuery) {
            this.updateQuery.abort()
        }
        if (this.deleteQuery) {
            this.deleteQuery.abort()
        }
    }

    render() {
        const id = this.state.id
        const name = this.state.name
        const image = this.state.image
        const categoryId = this.state.categoryId
        const categoryName = this.state.categoryName
        const categoryIcon = this.state.categoryIcon
        const modified = this.state.modified
        const validationErrors = this.state.validationErrors
        const categories = this.props.categories

        return (
            <div>
                <div className="row">
                    <div className="col s9 offset-s1">
                        Product Id: {id}
                    </div>
                </div>
                <div className="row edit-box valign-wrapper">
                    <div className="col s4" >
                        <ProductCard
                            name={name}
                            image={image}
                            categoryName={categoryName}
                            categoryIcon={categoryIcon}
                        />
                    </div>
                    <div className="col s8" >
                        <ProductForm
                            id={id}
                            name={name}
                            image={image}
                            categoryId={categoryId}
                            categories={categories}
                            handleSubmit={this.handleSubmit}
                            handleNameChanged={this.handleNameChanged}
                            handleImageChanged={this.handleImageChanged}
                            handleCategoryChanged={this.handleCategoryChanged}
                            handleDeleteClicked={this.handleDeleteClicked}
                            modified={modified}
                            validationErrors={validationErrors}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductEditBox