import React, { Component } from 'react'
import Input from "../../../support/materialize/Input";
import Select from "../../../support/materialize/Select";
import File from '../../../support/materialize/File'
import Button from "../../../support/materialize/Button";
import SubmitButton from "../../../support/materialize/SubmitButton";

class ProductForm extends React.Component {
    initializeDropDownSelect() {
        const elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {});
    }

    initializeFormLabels() {
        M.updateTextFields()
    }

    componentDidMount() {
        this.initializeDropDownSelect()
        this.initializeFormLabels()
    }

    render() {
        const validationErrors = this.props.validationErrors

        return (
            <div className="row edition-card">
                <form className="col s12"
                      action={"/products/" + this.props.id + "/edit"}
                      method="POST"
                      onSubmit={this.props.handleSubmit}
                >

                    <Input
                        col="s12"
                        type="text"
                        value={this.props.name}
                        onChange={this.props.handleNameChanged}
                        placeholder="Name ..."
                        label="Name"
                        validationError={validationErrors ? (validationErrors.name ? validationErrors.name : '') : undefined}

                    />
                    <Select
                        col="s12"
                        className="icons"
                        value={this.props.categoryId}
                        onChange={this.props.handleCategoryChanged}
                        options={this.props.categories}
                        nullOption={ {id:'', name: 'Choose a Category', icon: ''} }
                        getOptionValue={(category)=>{return category.id}}
                        getOptionText={(category)=>{return category.name}}
                        getOptionImage={(category)=>{return category.icon}}
                        getOptionClassName={(category)=>{return "left circle"}}
                        label="Category"
                        validationError={validationErrors ? (validationErrors.categoryId ? validationErrors.categoryId : '') : undefined}
                    />
                    <File
                        id={'imageFile-' + this.props.id}
                        col="s12"
                        onChange={this.props.handleImageChanged}
                        placeholder="Choose an image ..."
                        label="Image"
                        validationError={validationErrors ? (validationErrors.imageFile ? validationErrors.imageFile : '') : undefined}

                    />
                    <div className="col s3 offset-s5">
                        <Button
                            className="btn-small"
                            label="Delete"
                            onClick={this.props.handleDeleteClicked}
                            icon="delete"
                        />
                    </div>
                    <SubmitButton
                        col="s3"
                        className="btn-small"
                        label="Save"
                        disabled={!this.props.modified}
                    />
                </form>
            </div>
        );
    }
}

export default ProductForm;