import React, { Component } from 'react';
import {Link} from "react-router-dom";
import M from "materialize-css";
import {withRouter} from 'react-router-dom';

class Menu extends React.Component {
    initializeSideNav() {
        const elems = document.querySelectorAll('.sidenav');
        M.Sidenav.init(elems, {});
    }

    initializeCollapsible() {
        const collapsibleElem = document.querySelectorAll('.collapsible');
        M.Collapsible.init(collapsibleElem, {});
    }
    componentDidMount() {
        this.initializeCollapsible()
        this.initializeCollapsible()
    }

    render() {
        const path = this.props.match.path;
        const location = this.props.location.pathname;

        const usersListActive = location == path + '/users' ? 'active' : ''
        const usersAddActive = location == path + '/users/add' ? 'active' : ''
        const usersAccountActive = location == path + '/users/account' ? 'active' : ''

        const productsListActive = location == path + '/products' ? 'active' : ''
        const productsAddActive = location == path + '/products/add' ? 'active' : ''

        const categoriesListActive = location == path + '/categories' ? 'active' : ''
        const categoriesAddActive = location == path + '/categories/add' ? 'active' : ''

        return (
            <div>
                <ul id="slide-out" className="sidenav sidenav-fixed">
                    <li className="no-padding">
                        <ul className="collapsible collapsible-accordion">
                            <li>
                                <a className="collapsible-header">Users<i className="material-icons">account_circle</i></a>
                                <div className="collapsible-body">
                                    <ul>
                                        <li className={usersListActive}><Link to={path + '/users'}><i className="material-icons">people</i>List</Link></li>
                                        <li className={usersAddActive}><Link to={path + '/users/add'}><i className="material-icons">person_add</i>Add</Link></li>
                                        <li className={usersAccountActive}><Link to={path + '/users/account'}><i className="material-icons">settings</i>Account</Link></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li className="no-padding">
                        <ul className="collapsible collapsible-accordion">
                            <li>
                                <a className="collapsible-header">Products<i className="material-icons">local_mall</i></a>
                                <div className="collapsible-body">
                                    <ul>
                                        <li className={productsListActive}><Link to={path + '/products'}><i className="material-icons">dehaze</i>List</Link></li>
                                        <li className={productsAddActive}><Link to={path + '/products/add'}><i className="material-icons">add_circle</i>Add</Link></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li className="no-padding">
                        <ul className="collapsible collapsible-accordion">
                            <li>
                                <a className="collapsible-header">Categories<i className="material-icons">label_outline</i></a>
                                <div className="collapsible-body">
                                    <ul>
                                        <li className={categoriesListActive}><Link to={path + '/categories'}><i className="material-icons">dehaze</i>List</Link></li>
                                        <li className={categoriesAddActive}><Link to={path + '/categories/add'}><i className="material-icons">add_circle</i>Add</Link></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>


                <a href="#" data-target="slide-out" className="sidenav-trigger"><i
                    className="material-icons">menu</i></a>
            </div>
        );
    }
}

export default withRouter(Menu);