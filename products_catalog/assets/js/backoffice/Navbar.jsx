import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Authentication from "../models/Authentication";
import MaterializeNavbar from "../support/materialize/Navbar";

class Navbar extends React.Component {
    construct() {
        this.handleLogout = this.handleLogout.bind(this)
    }

    handleLogout(event) {
        event.preventDefault();

        Authentication.logout()
            .whenDataArrives( (response) => {
                Authentication.clearUser()
            })
    }

    render() {
        return (<MaterializeNavbar
           title="Products Catalog Admin"
           links={[
               <Link to='/' >Site</Link>,
               <a href="#" onClick={this.handleLogout} >Logout</a>
           ]}
        />)
    }
}

export default Navbar;