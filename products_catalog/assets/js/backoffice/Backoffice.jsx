import React, { Component } from 'react'
import {Switch, Route} from "react-router-dom"
import Navbar from "./Navbar"
import Menu from "./Menu"
import UsersList from "./users/list/UsersList"
import UsersAdd from "./users/add/UsersAdd"
import EditUser from "./users/account/EditUser"
import ProductsList from "./products/list/ProductsList"
import ProductsAdd from "./products/add/ProductsAdd"
import CategoriesList from "./categories/list/CategoriesList"
import CategoriesAdd from "./categories/add/CategoriesAdd"

class Backoffice extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="backoffice">
                <Navbar />
                <Menu />
                <div className="main">
                    <Switch>
                        <Route exact path="/admin/users" component={UsersList} />
                        <Route exact path="/admin/users/add" component={UsersAdd} />
                        <Route exact path="/admin/users/account" component={EditUser} />
                        <Route exact path="/admin/products" component={ProductsList} />
                        <Route exact path="/admin/products/add" component={ProductsAdd} />
                        <Route exact path="/admin/categories" component={CategoriesList} />
                        <Route exact path="/admin/categories/add" component={CategoriesAdd} />
                    </Switch>
                </div>
            </div>
        );
    }
}

export default Backoffice