import React, { Component } from 'react';
import Categories from "../../../models/backoffice/Categories";
import Input from '../../../support/materialize/Input'
import File from '../../../support/materialize/File'
import SubmitButton from '../../../support/materialize/SubmitButton'

class CategoriesAdd extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            icon: '',
            iconFile: undefined,
            validationErrors: undefined
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleNameChanged = this.handleNameChanged.bind(this)
        this.handleIconChanged = this.handleIconChanged.bind(this)
    }

    initializeFormLabels() {
        M.updateTextFields()
    }

    notifySuccessfullCreate() {
        M.toast({html: 'The category was created!'})
    }

    onSubmitResponse(response) {
        if (response.success) {
            this.notifySuccessfullCreate()

            this.setState({
                name: '',
                icon: '',
                validationErrors: undefined,
            })
        } else {
            this.setState({validationErrors: undefined})
            this.setState({validationErrors: response.errors})
        }
    }

    handleSubmit(event) {
        event.preventDefault()

        this.createQuery = Categories.create({
                name: this.state.name,
                iconFile: this.state.iconFile
        }).whenDataArrives( this.onSubmitResponse.bind(this));
    }

    handleNameChanged(event) {
        this.setState({
            name: event.target.value,
        })
    }

    handleIconChanged() {
        const iconFile = document.querySelector('#iconFile').files[0];

        this.setState({
            iconFile: iconFile,
        })
    }

    componentDidMount() {
        this.initializeFormLabels()
    }

    render() {
        const validationErrors = this.state.validationErrors

        return (
            <div className="row">
                <form className="col s8 offset-s1"
                      onSubmit={this.handleSubmit}
                >
                    <Input
                        col="s12"
                        type="text"
                        name="name"
                        value={this.state.name}
                        onChange={this.handleNameChanged}
                        placeholder="Name ..."
                        label="Name"
                        validationError={validationErrors ? (validationErrors.name ? validationErrors.name : '') : undefined}
                    />
                    <File
                        id="iconFile"
                        col="s12"
                        onChange={this.handleIconChanged}
                        placeholder="Choose an image ..."
                        label="Icon"
                        validationError={validationErrors ? (validationErrors.iconFile ? validationErrors.iconFile : '') : undefined}

                    />
                    <SubmitButton
                        col="s3 offset-s10"
                        className="btn-small"
                        label="Add"
                    />
                </form>
            </div>
        );
    }
}

export default CategoriesAdd;