import React, { Component } from 'react'
import Categories from "../../../models/backoffice/Categories"
import CategoryEditBox from './CategoryEditBox'

class CategoriesList extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            categories: [],
        }

        this.handleCategoryDeleted = this.handleCategoryDeleted.bind(this)
    }

    notifySuccessfullDelete() {
        M.toast({html: 'The category was deleted!'})
    }

    requestCategories() {
        this.categoriesQuery = Categories.searchFor('')
            .whenDataArrives( (response) => {
                this.setState({
                    categories: response.data
                })
            })
    }

    handleCategoryDeleted(deletedCategory) {
        const categoriesWithRemovedItem = this.state.categories.filter((category) => {
            return category.id != deletedCategory.id
        })

        this.setState({
            categories: categoriesWithRemovedItem,
        })

        this.notifySuccessfullDelete()
    }

    componentWillMount() {
        this.requestCategories()
    }

    componentWillUnmount() {
        if (this.categoriesQuery) {
            this.categoriesQuery.abort()
        }
    }

    render() {
        const categories = this.state.categories

        return categories.map( (category) => {
            return (
                <CategoryEditBox key={category.id}
                    id={category.id}
                    name={category.name}
                    icon={category.icon}
                    handleCategoryDeleted={this.handleCategoryDeleted}
                />
            )
        })
    }
}

export default CategoriesList