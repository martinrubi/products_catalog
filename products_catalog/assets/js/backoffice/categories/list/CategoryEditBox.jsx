import React, { Component } from 'react'
import Categories from "../../../models/backoffice/Categories"
import CategoryForm from './CategoryForm'
import Products from "../../../models/backoffice/Products";

class CategoryEditBox extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            id: props.id,
            name: props.name,
            icon: props.icon,
            modified: false,
            validationErrors: undefined
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleNameChanged = this.handleNameChanged.bind(this)
        this.handleIconChanged = this.handleIconChanged.bind(this)
        this.handleDeleteClicked = this.handleDeleteClicked.bind(this)
    }

    notifySuccessfullUpdate() {
        M.toast({html: 'The category was updated!'})
    }

    onSubmitResponse(response) {
        if (response.success) {
            this.notifySuccessfullUpdate()

            this.setState({validationErrors: []})
            this.setState({
                icon: response.data.icon,
                validationErrors: undefined,
                modified: false,
            })
        } else {
            this.setState({validationErrors: undefined})
            this.setState({validationErrors: response.errors})
        }
    }

    onCategoryDeleted(response) {
        if (response.success) {
            this.props.handleCategoryDeleted({
                id: this.props.id
            })
        } else {
            this.setState({
                validationErrors: response.errors,
            })
        }
    }

    handleSubmit(event) {
        event.preventDefault()

        this.updateQuery = Categories.update(
            {
                id: this.state.id,
                name: this.state.name,
                iconFile: this.state.iconFile,
            }
        ).whenDataArrives(this.onSubmitResponse.bind(this));
    }

    handleNameChanged(event) {
        this.setState({
            name: event.target.value,
            modified: true,
        })
    }

    handleIconChanged(event) {
        const inputSelector = '#iconFile-' + this.state.id
        const iconFile = document.querySelector(inputSelector).files[0];

        this.setState({
            iconFile: iconFile,
            modified: true,
        })
    }

    handleDeleteClicked() {
        this.deleteQuery = Categories.remove({
            id: this.state.id
        }).whenDataArrives(this.onCategoryDeleted.bind(this));
    }

    componentWillUnmount() {
        if (this.updateQuery) {
            this.updateQuery.abort()
        }
        if (this.deleteQuery) {
            this.deleteQuery.abort()
        }
    }

    render() {
        const id = this.state.id
        const name = this.state.name
        const icon = this.state.icon
        const modified = this.state.modified
        const validationErrors = this.state.validationErrors

        return (
            <div>
                <div className="row">
                    <div className="col s11 offset-s1">
                        Category Id: {id}
                    </div>
                </div>
                <div className="row edit-box valign-wrapper">
                    <div className="col s1" >
                        <div className="row">
                            <div className="col s4">
                                <img className="categoryIcon" src={icon} />
                            </div>
                            <div className="col s12 valign-wrapper">
                                {name}
                            </div>
                        </div>
                    </div>
                    <div className="col s11" >
                        <CategoryForm
                            id={id}
                            name={name}
                            icon={icon}
                            handleSubmit={this.handleSubmit}
                            handleNameChanged={this.handleNameChanged}
                            handleIconChanged={this.handleIconChanged}
                            handleDeleteClicked={this.handleDeleteClicked}
                            modified={modified}
                            validationErrors={validationErrors}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default CategoryEditBox