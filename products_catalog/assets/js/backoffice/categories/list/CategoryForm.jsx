import React, { Component } from 'react'
import Input from '../../../support/materialize/Input'
import SubmitButton from '../../../support/materialize/SubmitButton'
import File from "../../../support/materialize/File";
import Button from "../../../support/materialize/Button";

class CategoryForm extends React.Component {
    initializeFormLabels() {
        M.updateTextFields()
    }

    componentDidMount() {
        this.initializeFormLabels()
    }

    render() {
        const validationErrors = this.props.validationErrors

        return (
            <div className="row edition-card">
                <form className="col s12"
                      onSubmit={this.props.handleSubmit}
                >
                    <Input
                        col="s9"
                        type="text"
                        value={this.props.name}
                        onChange={this.props.handleNameChanged}
                        placeholder="Name ..."
                        label="Category name:"
                        validationError={validationErrors ? (validationErrors.name ? validationErrors.name : '') : undefined}

                    />
                    <File
                        id={'iconFile-' + this.props.id}
                        col="s9"
                        onChange={this.props.handleIconChanged}
                        placeholder="Choose an image ..."
                        label="Icon"
                        validationError={validationErrors ? (validationErrors.iconFile ? validationErrors.iconFile : '') : undefined}

                    />
                    <div className="col s3 offset-s4">
                        <Button
                            className="btn-small"
                            label="Delete"
                            onClick={this.props.handleDeleteClicked}
                            icon="delete"
                        />
                    </div>
                    <SubmitButton
                        col="s3"
                        label="Save"
                        disabled={!this.props.modified}
                    />
                </form>
            </div>
        );
    }
}

export default CategoryForm;