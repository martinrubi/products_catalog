import React, { Component } from 'react';
import {Route, Redirect, withRouter} from "react-router-dom"
import Authentication from "../models/Authentication";
import If from "../support/conditionals/If";

/**
 * A Route that requires authorization.
 * If the user is authenticated allows the Route, otherwise redirects to the Login component.
 */
class AuthorizedRoute extends React.Component {
    render() {
        return <If
            condition={Authentication.isAuthenticated()}
            render={
                () => <Route path={this.props.path} component={this.props.component} />
            }
            else={
                () => <Redirect to={'/login?redirectTo=' + this.props.location.pathname} />
            }
        />
    }
}

export default withRouter(AuthorizedRoute);