import React, { Component } from 'react'
import {Redirect, withRouter} from "react-router-dom"
import queryString from 'query-string'
import Navbar from "../frontend/Navbar"
import Authentication from "../models/Authentication";
import Input from '../support/materialize/Input'
import SubmitButton from '../support/materialize/SubmitButton'

/**
 * The login form.
 */
class Login extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            redirectUrl: undefined,
            validationErrors: undefined,
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUsernameChanged = this.handleUsernameChanged.bind(this)
        this.handlePasswordChanged = this.handlePasswordChanged.bind(this)
    }

    initializeFormLabels() {
        M.updateTextFields()
    }

    getRedirectUrl() {
        const queryParams = queryString.parse(this.props.location.search)
        return queryParams.redirectTo === undefined ? '/admin/products' : queryParams.redirectTo
    }

    onSubmitResponse(response) {
        if (response.success) {
            this.handleLoggedIn(response.data)
        } else {
            this.setState({validationErrors: undefined})
            this.setState({validationErrors: response.errors})
        }
    }

    handleSubmit(event) {
        event.preventDefault()

        this.loginQuery = Authentication.login({
            username: this.state.username,
            password: this.state.password
        }).whenDataArrives(this.onSubmitResponse.bind(this));
    }

    handleLoggedIn(data) {
        Authentication.storeUser(data)

        this.setState({
            redirectUrl: this.getRedirectUrl()
        })
    }

    handleUsernameChanged() {
        this.setState({
            username: event.target.value,
        })
    }

    handlePasswordChanged() {
        this.setState({
            password: event.target.value,
        })
    }

    componentDidMount() {
        this.initializeFormLabels()
    }

    componentWillUnmount() {
        if (this.loginQuery) {
            this.loginQuery.abort()
        }
    }

    render() {
        const redirectUrl = this.state.redirectUrl

        if (redirectUrl) {
            return (
                <Redirect to={redirectUrl} />
            )
        }

        const validationErrors = this.state.validationErrors

        return (
            <div className="frontend">
                <Navbar />
                <div className="row">
                    <form className="col s6 offset-s3"
                          onSubmit={this.handleSubmit}
                    >
                        <Input
                            col="s12"
                            name="username"
                            type="text"
                            value={this.state.username}
                            onChange={this.handleUsernameChanged}
                            placeholder="Username ..."
                            label="Username"
                            validationError={validationErrors ? (validationErrors.username ? validationErrors.username : '') : undefined}
                        />
                        <Input
                            col="s12"
                            name="password"
                            type="password"
                            value={this.state.password}
                            onChange={this.handlePasswordChanged}
                            placeholder="Password ..."
                            label="Password"
                            validationError={validationErrors ? (validationErrors.password ? validationErrors.password : '') : undefined}
                        />
                        <SubmitButton
                            col="s3 offset-s10"
                            label="Login"
                        />
                    </form>
                </div>
            </div>
        )
    }
}

export default withRouter(Login);