import RemoteQuery from '../RemoteQuery';
import Authentication from "../Authentication";

/**
 * Interface for the backoffice to work with Products.
 *
 * Examples:
 *
 *      const query = Products.seachFor('paintings')
 *          .whenDataArrives( (response) => {
 *              this.setState({
 *                  products: response.data
 *              })
 *          })
 *
 *  The query can be canceled with:
 *
 *      query.abort()
 */
class Products
{
    static searchFor(q) {
        const url = '/apiv1/admin/products?q=' + q;

        return new RemoteQuery(url, {
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }

    static create(product) {
        const url = '/apiv1/admin/products/create';

        var formData = new FormData();

        formData.append( 'product[name]', product.name );
        formData.append( 'product[categoryId]', product.categoryId );
        formData.append( 'productImage', product.imageFile );

        return new RemoteQuery(url, {
            method: "POST",
            body: formData,
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }

    static update(product)
    {
        const url = '/apiv1/admin/products/' + product.id;

        var formData = new FormData();

        formData.append( 'product[name]', product.name );
        formData.append( 'product[categoryId]', product.categoryId );
        formData.append( 'productImage', product.imageFile );

        return new RemoteQuery(url, {
            method: "POST",
            body: formData,
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }

    static remove(product)
    {
        const url = '/apiv1/admin/products/' + product.id;

        return new RemoteQuery(url, {
            method: "DELETE",
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }
}

export default Products;