import RemoteQuery from '../RemoteQuery';
import Authentication from "../Authentication";

/**
 * Interface for the backoffice to work with Users.
 *
 * Examples:
 *
 *      const query = Users.seachFor('Lisa')
 *          .whenDataArrives( (response) => {
 *              this.setState({
 *                  products: response.data
 *              })
 *          })
 *
 *  The query can be canceled with:
 *
 *      query.abort()
 */
class Users
{
    static searchFor(q) {
        const url = '/apiv1/admin/users?q=' + q;

        return new RemoteQuery(url, {
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }

    static create(user) {
        const url = '/apiv1/admin/users/create';

        var formData = new FormData();

        formData.append( 'user[username]', user.username );
        formData.append( 'user[email]', user.email );
        formData.append( 'user[password]', user.password );

        return new RemoteQuery(url, {
            method: "POST",
            body: formData,
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }

    static update(user) {
        const userId = Authentication.getUser().id

        const url = '/apiv1/admin/users/' + userId;

        var formData = new FormData();

        formData.append( 'user[username]', user.username );

        if( user.password ) {
            formData.append( 'user[password]', user.password );
        }

        return new RemoteQuery(url, {
            method: "POST",
            body: formData,
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }
}

export default Users;