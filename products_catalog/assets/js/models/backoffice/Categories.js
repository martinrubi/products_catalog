import RemoteQuery from '../RemoteQuery';
import Authentication from "../Authentication";

/**
 * Interface for the backoffice to work with Categories.
 *
 * Examples:
 *
 *      const query = Categories.all()
 *          .whenDataArrives( (response) => {
 *              // do something with response.data
 *          })
 *
 *  The query can be canceled with:
 *
 *      query.abort()
 */
class Categories
{
    /**
     * Returns all the categories:
     *      [
     *          {
     *              id: int,
     *              name: string,
     *              image: url
     *          }
     *      ]
     *
     * @returns {RemoteQuery}
     */
    static searchFor(q) {
        const url = '/apiv1/admin/categories?q=' + q;

        return new RemoteQuery(url, {
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }

    static create(category) {
        const url = '/apiv1/admin/categories/create';

        var formData = new FormData();

        formData.append( 'category[name]', category.name );
        formData.append( 'categoryIcon', category.iconFile );

        return new RemoteQuery(url, {
            method: "POST",
            body: formData,
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }

    static update(category)
    {
        const url = '/apiv1/admin/categories/' + category.id;

        var formData = new FormData();

        formData.append( 'category[name]', category.name );
        formData.append( 'categoryIcon', category.iconFile );

        return new RemoteQuery(url, {
            method: "POST",
            body: formData,
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }

    static remove(category)
    {
        const url = '/apiv1/admin/categories/' + category.id;

        return new RemoteQuery(url, {
            method: "DELETE",
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }
}

export default Categories;