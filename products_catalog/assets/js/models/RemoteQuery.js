import Authentication from './Authentication'

/**
 * A cancelable fetch to a server.
 *
 * Example:
 *      const query = new RemoteQuery('/users')
 *          .whenDataArrives( (response) => {
 *              // ... do something with response.data
 *          })
 *
 *  The query can be canceled with:
 *
 *      query.abort()
 */
class RemoteQuery
{
    constructor(url, init = {}) {
        this.controller = new AbortController()

        init.signal = this.controller.signal

        this.request = fetch(url, init)
    }

    whenDataArrives(callback) {
        this.request
            .then((response) => {
                if (response.status == 403) {
                    this.onAuthenticationFailed()
                    return 'authenticationFailed'
                }

                return response.json()
            })
            .then( (response) => {
                if(response === 'authenticationFailed' ) {
                    return undefined
                }

                callback(response)
            }).catch( (response) => {
                console.log(response)
            })

        return this
    }

    abort() {
        this.controller.abort()
    }

    onAuthenticationFailed() {
        Authentication.clearUser()
    }
}

export default RemoteQuery