import RemoteQuery from '../RemoteQuery';

/**
 * Interface for working with Products.
 *
 * Examples:
 *
 *      const query = Products.seachFor('paintings')
 *          .whenDataArrives( (response) => {
 *              this.setState({
 *                  products: response.data
 *              })
 *          })
 *
 *  The query can be canceled with:
 *
 *      query.abort()
 */
class Products
{
    static searchFor(q) {
        const url = '/apiv1/products?q=' + q;

        return new RemoteQuery(url, {});
    }
}

export default Products;