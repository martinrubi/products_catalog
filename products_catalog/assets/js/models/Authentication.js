import RemoteQuery from './RemoteQuery';

class Authentication
{
    /**
     * Store the session user provided by the server for later requests.
     */
    static storeUser(user) {
        localStorage.setItem('logged_user', JSON.stringify(user))
    }

    /**
     * Get the session user.
     */
    static getUser() {
        return JSON.parse(localStorage.getItem('logged_user'))
    }

    /**
     * Drop the session api token and call the onUnauthenticatedAccess callback to inform that the
     * session has ended.
     */
    static clearUser() {
        localStorage.removeItem('logged_user')

        Authentication.onUnauthenticatedAccess()
    }

    /**
     * Get the current session api token.
     */
    static getToken() {
        return this.getUser().token
    }

    /**
     * Return true if currently there is a user logged with a valid session api token.
     * @returns {boolean}
     */
    static isAuthenticated() {
        return this.getUser() != null
    }

    /**
     * Login to the server and keep the session apiToken.
     */
    static login(credentials) {
        const url = '/apiv1/login';

        let formData = new FormData();

        formData.append( '_username', credentials.username );
        formData.append( '_password', credentials.password );

        return new RemoteQuery(url, {
            method: "POST",
            body: formData
        });
    }

    /**
     * Logout from the current session.
     */
    static logout(credentials) {
        const url = '/apiv1/admin/logout';

        return new RemoteQuery(url, {
            method: "POST",
            headers: {
                'X-Auth-Token': Authentication.getToken(),
            },
        });
    }
}

Authentication.onUnauthenticatedAccess = null;

export default Authentication;