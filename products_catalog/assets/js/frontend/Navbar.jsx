import React, { Component } from 'react';
import {Link} from "react-router-dom";
import MaterializeNavbar from "../support/materialize/Navbar";

class Navbar extends React.Component {
    render() {
        return (
            <MaterializeNavbar
                title="Products Catalog"
                links={[
                    <Link to='/admin/products'>Admin</Link>
                ]}
            />
        );
    }
}

export default Navbar;