import React, { Component } from 'react';

import Navbar from './Navbar'
import ProductsCatalog from './ProductsSearch/ProductsCatalog'

class Frontend extends React.Component {
    render() {
        return (
            <div className="frontend">
                <Navbar />
                <ProductsCatalog />
            </div>
        );
    }
}

export default Frontend;