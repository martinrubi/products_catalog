import React, { Component } from 'react';
import Products from '../../models/frontend/Products';
import Input from '../../support/materialize/Input'

class SearchProducts extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            q: ''
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    requestProducts() {
        const q = this.state.q;

        this.productsQuery = Products.searchFor(q)
            .whenDataArrives( (response) => {
                this.props.onProductsFound(response.data)
            });
    }

    handleChange(event) {
        this.setState({q: event.target.value})
    }

    handleSubmit(event) {
        event.preventDefault()

        this.requestProducts()
    }

    componentWillMount() {
        this.requestProducts()
    }

    componentWillUnmount() {
        if (this.productsQuery) {
            this.productsQuery.abort()
        }
    }

    render() {
        return (
            <div className="row">
                <form className="col s6 offset-s3" action="/products" method="GET" onSubmit={this.handleSubmit}>
                    <Input
                        col="s12"
                        id="search-products"
                        name="q"
                        type="text"
                        value={this.state.value}
                        onChange={this.handleChange}
                        placeholder="Search for products ..."
                        leftIcon="search"
                    />
                </form>
            </div>
        )
    }
}

export default SearchProducts