import React, { Component } from 'react';
import SearchProducts from "./SearchProducts";
import ProductsList from "./ProductsList";

class ProductsCatalog extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            products: []
        }
    }

    onProductsFound(products) {
        this.setState({
            products: products
        });
    }

    render() {
        const products = this.state.products;

        return (
            <div className="row">
                <div className="col s12" >
                    <SearchProducts onProductsFound={(products) => this.onProductsFound(products)} />
                    <ProductsList products={products}/>
                </div>
            </div>
        );
    }
}

export default ProductsCatalog;