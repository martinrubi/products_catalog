import React, { Component } from 'react';
import ProductCard from "./ProductCard";

class ProductsList extends React.Component {
    renderProducts() {
        const products = this.props.products;

        return products.map( (product) => {
            return (
                <ProductCard
                    key={product.id}
                    name={product.name}
                    image={product.image}
                    categoryName={product.category.name}
                    categoryIcon={product.category.icon}
                />
            );
        })
    }

    render() {
        const $productCards = this.renderProducts();

        return (
            <div className="row">
                <div className="col s8 offset-l2" >
                    {$productCards}
                </div>
            </div>
        );
    }
}

export default ProductsList;