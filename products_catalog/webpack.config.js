// webpack.config.js
var Encore = require('@symfony/webpack-encore');

Encore
// the project directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    // will create web/build/app.js and web/build/app.css
    .addEntry('app', './assets/js/app.js')

    // allow sass/scss files to be processed
    .enableSassLoader()

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    .enableReactPreset()

    .enableSingleRuntimeChunk()

// create hashed filenames (e.g. app.abc123.css)
// .enableVersioning()
;

// Use polling instead of inotify because we are running in Vagrant.
// See https://github.com/symfony/webpack-encore/issues/191
const config = Encore.getWebpackConfig();
config.watchOptions = {
    poll: true,
};

// export the final configuration
module.exports = config;