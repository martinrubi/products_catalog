<?php
declare(strict_types=1);

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Haijin\Bridge\SpecsInSymfony;

SpecsInSymfony::addTo($specs);


$specs->beforeAll(function (){
    $this->resetDatabase();
});

$specs->beforeEach(function (){
    $this->resetKernel();
});

$specs->afterAll(function (){
    $this->resetDatabase();

    if(isset($this->em)) {
        $this->em->close();
    }
});

$specs->let('em', function (){
    return $this->container->get('doctrine')->getManager();
});

$specs->let('usersRepository', function (){
    return $this->em->getRepository(User::class);
});

$specs->let('productsRepository', function (){
    return $this->em->getRepository(Product::class);
});

$specs->let('categoriesRepository', function (){
    return $this->em->getRepository(Category::class);
});

$specs->let('testImageFile', function (){
    // Copy the image file to a tmp folder
    copy ( 'tests/test-image.png', 'var/cache/test/test-image.png');

    // Use the tmp image file
    $filename = 'var/cache/test/test-image.png';

    return new UploadedFile(
        $filename,
        'test-image.png',
        'image/png',
        filesize($filename)
    );
});


$specs->def('resetDatabase', function (){
    $this->productsRepository->deleteAll();
    $this->categoriesRepository->deleteAll();
    $this->usersRepository->deleteAll();

    $this->em->flush();

    $this->em->clear();
});

$specs->def('createDefaultUser', function (){
    $defaultUser = $this->usersRepository->findOneBy(['username' => 'test_user']);

    if ($defaultUser != null) {
        return;
    }

    $defaultUser = new User('test_user', 'test_user@user.com', '123456');

    $this->usersRepository->persist($defaultUser);

    $this->em->flush();

    $this->em->clear();
});

$specs->def('loginWithDefaultUser', function (){
    $this->createDefaultUser();

    $this->request('POST',
        '/apiv1/login', [
        '_username' => 'test_user',
        '_password' => '123456',
    ]);

    if(!isset($this->getJsonResponse()['data']['token'])) {
        return null;
    }

    return $this->getJsonResponse()['data']['token'];
});