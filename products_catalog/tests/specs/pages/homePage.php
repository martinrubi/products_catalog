<?php
declare(strict_types=1);

$spec->describe('When navigating to the home page /', function() {

    $this->it('returns a 200 status code', function() {

        $this->request('GET', '/');

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

});