<?php
declare(strict_types=1);

// This is to make sure that Symfony is delegating all the urls to React and not just '/'
$spec->describe('When navigating to any page, such as /products', function() {

    $this->it('returns a 200 status code', function() {

        $this->request('GET', '/products');

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

});