<?php
declare(strict_types=1);

use AppBundle\Entity\Category;

$spec->describe('When creating a user using the endpoint "/apiv1/admin/users/create"', function() {

    $this->beforeEach(function (){
        $this->resetDatabase();
    });

    $this->def('createUser', function($apiToken, $body) {
        $this->request('POST',
            '/apiv1/admin/users/create',
            $body,
            [],
            ['HTTP_X-Auth-Token' => $apiToken]
        );
    });

    $this->describe('with an invalid api token', function (){

        $this->it('returns a 403 status code', function() {

            $this->createUser(null, [
                'user' => [
                    'username' => 'new name',
                    'email' => 'new_name@user.com',
                    'password' => '123456',
                ],
            ]);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });

    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createUser($apiToken, [
            'user' => [
                'username' => 'new name',
                'email' => 'new_name@user.com',
                'password' => '123456',
            ],
        ]);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);

    });

    $this->it('returns the user created', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createUser($apiToken, [
            'user' => [
                'username' => 'new name',
                'email' => 'new_name@user.com',
                'password' => '123456',
            ],
        ]);

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                'id' => 2,
                'username' => 'new name',
                'email' => 'new_name@user.com',
            ]
        ]);
    });

    $this->it('creates the user in the database', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createUser($apiToken, [
            'user' => [
                'username' => 'new name',
                'email' => 'new_name@user.com',
                'password' => '123456',
            ],
        ]);

        $user = $this->usersRepository->find(2);

        $this->expect($user) ->to() ->be() ->exactlyLike([
            'getId()' => 2,
            'getUsername()' => 'new name',
            'getEmail()' => 'new_name@user.com',
            'getPassword()' => function($password) use($user){
                $this->expect($user->isValidPassword('123456')) ->to() ->be() ->true();
            },
        ]);
    });

    $this->describe('with invalid parameters', function () {

        $this->let( 'validParams', function () {
            return [
                'user' => [
                    'username' => 'new name',
                    'email' => 'new_name@user.com',
                    'password' => '123456',
                ],
            ];
        });

        $this->it('fails when the username is missing', function() {
            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            unset($params['user']['username']);

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'username' => 'Key username must be present'
                ]
            ]);
        });

        $this->it('fails when the username is empty', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['user']['username'] = '';

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'username' => 'username must not be optional'
                ]
            ]);
        });

        $this->it('fails when the username is blank', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['user']['username'] = '  ';

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'username' => 'username must not be blank'
                ]
            ]);
        });

        $this->it('fails when the username is too long', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['user']['username'] = str_repeat('a', 256);

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'username' => 'username must have a length between 1 and 255'
                ]
            ]);
        });

        $this->it('fails when the username already exists', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['user']['username'] = 'test_user';

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'username' => 'The username already exists.'
                ]
            ]);
        });

        $this->it('fails when the email is missing', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            unset($params['user']['email']);

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'email' => 'Key email must be present'
                ]
            ]);
        });

        $this->it('fails when the email is empty', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['user']['email'] = '';

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'email' => 'email must not be optional'
                ]
            ]);
        });

        $this->it('fails when the username is blank', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['user']['email'] = '  ';

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'email' => 'email must not be blank'
                ]
            ]);
        });

        $this->it('fails when the username is too long', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['user']['email'] = str_repeat('a', 256);

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'email' => 'email must have a length between 1 and 255'
                ]
            ]);
        });

        $this->it('fails when the username already exists', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['user']['email'] = 'test_user@user.com';

            $this->createUser($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'email' => 'The email already exists.'
                ]
            ]);
        });

    });
});