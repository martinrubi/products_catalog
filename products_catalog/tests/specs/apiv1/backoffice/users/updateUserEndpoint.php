<?php
declare(strict_types=1);

use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use Haijin\Bridge\SpecsInSymfony;

$spec->describe('When updating a user using the endpoint "/apiv1/admin/users/{user_id}"', function() {

    $this->beforeEach(function (){
        $this->resetDatabase();

        $this->usersRepository->persist(
            new User('User 1', 'user1@user.com', '123456')
        );

        $this->usersRepository->persist(
            new User('User 2', 'user2@user.com', '123456')
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->def('updateUser', function($apiToken, $userId, $body) {
        $this->request('POST',
            "/apiv1/admin/users/$userId",
            $body,
            [],
            ['HTTP_X-Auth-Token' => $apiToken]
        );
    });

    $this->describe('with an invalid api token', function (){

        $this->it('returns a 403 status code', function() {

            $this->updateUser(null, 3, [
                'user' => [
                    'username' => 'new name',
                    'password' => '123456',
                ],
            ]);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });

    });

    $this->describe('without updating its password', function() {

        $this->it('returns a 200 status code', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateUser($apiToken, 3, [
                'user' => [
                    'username' => 'new name',
                ],
            ]);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);

        });

        $this->it('returns the user created', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateUser($apiToken, 3, [
                'user' => [
                    'username' => 'new name',
                ],
            ]);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => true,
                'data' => [
                    'id' => 3,
                    'username' => 'new name',
                    'email' => 'test_user@user.com',
                ]
            ]);
        });

        $this->it('creates the user in the database', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateUser($apiToken, 3, [
                'user' => [
                    'username' => 'new name',
                ],
            ]);

            $user = $this->usersRepository->find(3);

            $this->expect($user) ->to() ->be() ->exactlyLike([
                'getId()' => 3,
                'getUsername()' => 'new name',
                'getEmail()' => 'test_user@user.com',
                'getPassword()' => function($password) { $this->expect($password) ->not() ->to() ->equal(''); },
            ]);
        });

        $this->describe('with invalid parameters', function () {

            $this->let( 'validParams', function () {
                return [
                    'user' => [
                        'username' => 'new name',
                    ],
                ];
            });

            $this->it('fails when the username is missing', function() {
                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                unset($params['user']['username']);

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'Key username must be present'
                    ]
                ]);
            });

            $this->it('fails when the username is empty', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['username'] = '';

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'username must not be optional'
                    ]
                ]);
            });

            $this->it('fails when the username is blank', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['username'] = '  ';

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'username must not be blank'
                    ]
                ]);
            });

            $this->it('fails when the username is too long', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['username'] = str_repeat('a', 256);

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'username must have a length between 1 and 255'
                    ]
                ]);
            });

            $this->it('fails when the username already exists', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['username'] = 'User 1';

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'The username already exists.'
                    ]
                ]);
            });

        });

    });

    $this->describe('updating its password', function() {

        $this->it('returns a 200 status code', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateUser($apiToken, 3, [
                'user' => [
                    'username' => 'new name',
                    'password' => '123456789',
                ],
            ]);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);

        });

        $this->it('returns the user updated', function() {
            $apiToken = $this->loginWithDefaultUser();

            $this->updateUser($apiToken, 3, [
                'user' => [
                    'username' => 'new name',
                    'password' => '123456789',
                ],
            ]);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => true,
                'data' => [
                    'id' => 3,
                    'username' => 'new name',
                    'email' => 'test_user@user.com',
                ]
            ]);
        });

        $this->it('updates the user in the database', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateUser($apiToken, 3, [
                'user' => [
                    'username' => 'new name',
                    'password' => '123456789',
                ],
            ]);

            $user = $this->usersRepository->find(3);

            $this->expect($user) ->to() ->be() ->exactlyLike([
                'getId()' => 3,
                'getUsername()' => 'new name',
                'getEmail()' => 'test_user@user.com',
                'getPassword()' => function($password) use($user){
                        $this->expect($user->isValidPassword('123456789')) ->to() ->be() ->true();
                    },
            ]);
        });

        $this->describe('with invalid parameters', function () {

            $this->let( 'validParams', function () {
                return [
                    'user' => [
                        'username' => 'new name',
                        'password' => '123456789',
                    ],
                ];
            });

            $this->it('fails when the username is missing', function() {
                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                unset($params['user']['username']);

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'Key username must be present'
                    ]
                ]);
            });

            $this->it('fails when the username is empty', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['username'] = '';

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'username must not be optional'
                    ]
                ]);
            });

            $this->it('fails when the username is blank', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['username'] = '  ';

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'username must not be blank'
                    ]
                ]);
            });

            $this->it('fails when the username is too long', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['username'] = str_repeat('a', 256);

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'username must have a length between 1 and 255'
                    ]
                ]);
            });

            $this->it('fails when the username already exists', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['username'] = 'User 1';

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'username' => 'The username already exists.'
                    ]
                ]);
            });

            $this->it('fails when the password is blank', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['password'] = '      ';

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'password' => 'password must not be blank'
                    ]
                ]);
            });

            $this->it('fails when the password is too short', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['password'] = '123';

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'password' => 'password must have a length between 6 and 255'
                    ]
                ]);
            });

            $this->it('fails when the password is too long', function() {

                $apiToken = $this->loginWithDefaultUser();

                $params = $this->validParams;

                $params['user']['password'] = str_repeat('a', 256);

                $this->updateUser($apiToken, 3, $params);

                $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                    'success' => false,
                    'errors' => [
                        'password' => 'password must have a length between 6 and 255'
                    ]
                ]);
            });

        });

    });

});