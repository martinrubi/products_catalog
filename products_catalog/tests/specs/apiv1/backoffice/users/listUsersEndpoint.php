<?php
declare(strict_types=1);

use AppBundle\Entity\User;

$spec->describe('When getting the users from the endpoint "/apiv1/admin/users"', function() {

    $this->beforeAll(function (){
        $this->resetDatabase();

        $this->usersRepository->persist(
            new User('User 1', 'user1@user.com', '123456')
        );

        $this->usersRepository->persist(
            new User('User 2', 'user2@user.com', '123456')
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->let('endpointUrl', function () {
        return '/apiv1/admin/users';
    });

    $this->describe('with an invalid api token', function (){
        $this->it('returns a 403 status code', function() {

            $this->request('GET', $this->endpointUrl);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);
            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });
    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->request('GET',
            $this->endpointUrl, [], [], ['HTTP_X-Auth-Token' => $apiToken]
        );

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns the users list', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->request('GET',
            $this->endpointUrl, [], [], ['HTTP_X-Auth-Token' => $apiToken]
        );

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                [
                    'id' => 3,
                    'username' => 'test_user',
                    'email' => 'test_user@user.com',
                ],
                [
                    'id' => 1,
                    'username' => 'User 1',
                    'email' => 'user1@user.com',
                ],
                [
                    'id' => 2,
                    'username' => 'User 2',
                    'email' => 'user2@user.com',
                ],
            ]
        ]);
    });

    $this->it('returns the matching users', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->request('GET',
            $this->endpointUrl . '?q=2', [], [], ['HTTP_X-Auth-Token' => $apiToken]
        );

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                [
                    'id' => 2,
                    'username' => 'User 2',
                    'email' => 'user2@user.com',
                ],
            ],
        ]);
    });

});