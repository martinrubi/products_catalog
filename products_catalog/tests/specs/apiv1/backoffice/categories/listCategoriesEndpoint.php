<?php
declare(strict_types=1);

use AppBundle\Entity\Category;

$spec->describe('When getting the categories from the endpoint "/apiv1/admin/categories"', function() {

    $this->beforeAll(function (){
        $this->resetDatabase();

        $this->categoriesRepository->persist(
            new Category('Category 1', 'test-image.png')
        );

        $this->categoriesRepository->persist(
            new Category('Category 2', 'test-image.png')
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->def('listCategories', function ($apiToken, $q=null) {
        $this->request('GET',
            "/apiv1/admin/categories?q=$q", [], [], ['HTTP_X-Auth-Token' => $apiToken]
        );
    });

    $this->describe('with an invalid api token', function (){
        $this->it('returns a 403 status code', function() {

            $this->listCategories(null);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);
            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });
    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->listCategories($apiToken);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns the categories list', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->listCategories($apiToken);

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                [
                    'id' => 1,
                    'name' => 'Category 1',
                    'icon' => '/images/categories/test-image.png',
                ],
                [
                    'id' => 2,
                    'name' => 'Category 2',
                    'icon' => '/images/categories/test-image.png',
                ],
            ],
        ]);
    });

    $this->it('returns the matching categories', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->listCategories($apiToken, 'Category 2');

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                [
                    'id' => 2,
                    'name' => 'Category 2',
                    'icon' => '/images/categories/test-image.png',
                ],
            ],
        ]);
    });

});