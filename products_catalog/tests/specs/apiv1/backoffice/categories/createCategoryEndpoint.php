<?php
declare(strict_types=1);

use AppBundle\Entity\Category;

$spec->describe('When creating a category using the endpoint "/apiv1/admin/categories/create"', function() {

    $this->beforeEach(function (){
        $this->resetDatabase();
    });

    $this->def('createCategory', function ($apiToken, $bodyData, $imageFile=null) {
        $this->request('POST',
            '/apiv1/admin/categories/create',
            $bodyData,
            ($imageFile ? [ 'categoryIcon' => $this->testImageFile ] : []),
            ['HTTP_X-Auth-Token' => $apiToken]
        );
    });

    $this->describe('with an invalid api token', function (){

        $this->it('returns a 403 status code', function() {

            $this->createCategory(null, [
                'category' => [
                    'name' => 'new name',
                ]
            ], $this->testImageFile);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });

    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createCategory($apiToken, [
            'category' => [
                'name' => 'new name',
            ]
        ], $this->testImageFile);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns the category created', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createCategory($apiToken, [
            'category' => [
                'name' => 'new name',
            ]
        ], $this->testImageFile);

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                'id' => 1,
                'name' => 'new name',
                'icon' => '/images/categories/test-image.png',
            ]
        ]);
    });

    $this->it('creates the category in the database', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createCategory($apiToken, [
            'category' => [
                'name' => 'new name',
            ]
        ], $this->testImageFile);

        $category = $this->categoriesRepository->find(1);

        $this->expect($category) ->to() ->be() ->exactlyLike([
            'getName()' => 'new name',
            'getIcon()' => 'test-image.png',
        ]);
    });

    $this->describe('with invalid parameters', function () {

        $this->it('fails when the name is missing', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->createCategory($apiToken, [
                'category' => [
                ]
            ], $this->testImageFile);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'Key name must be present'
                ]
            ]);
        });

        $this->it('fails when the name is empty', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->createCategory($apiToken, [
                'category' => [
                    'name' => '',
                ]
            ], $this->testImageFile);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must not be optional'
                ]
            ]);
        });

        $this->it('fails when the name is blank', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->createCategory($apiToken, [
                'category' => [
                    'name' => '  ',
                ]
            ], $this->testImageFile);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must not be blank'
                ]
            ]);
        });

        $this->it('fails when the name is too long', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->createCategory($apiToken, [
                'category' => [
                    'name' => str_repeat('a', 256),
                ]
            ], $this->testImageFile);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must have a length between 1 and 255'
                ]
            ]);
        });

        $this->it('fails when the icon is missing', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->createCategory($apiToken, [
                'category' => [
                    'name' => 'Category',
                ]
            ], null);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'iconFile' => 'iconFile must not be optional'
                ]
            ]);
        });

    });
});