<?php
declare(strict_types=1);

use AppBundle\Entity\Category;

$spec->describe('When updating a category using the endpoint "/apiv1/admin/categories/{category_id}"', function() {

    $this->beforeEach(function (){
        $this->resetDatabase();

        $this->categoriesRepository->persist(
            new Category('Category 1', 'image.png')
        );

        $this->categoriesRepository->persist(
            new Category('Category 2', 'image.png')
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->def('updateCategory', function($apiToken, $categoryId, $body, $testImageFile=null){
        $this->request('POST',
            "/apiv1/admin/categories/$categoryId",
            $body,
            [
                'categoryIcon' => $testImageFile,
            ],
            [
                'HTTP_X-Auth-Token' => $apiToken
            ]
        );

    });

    $this->describe('with an invalid api token', function (){
        $this->it('returns a 403 status code', function() {

            $this->updateCategory(
                null,
                2,
                [
                    'category' => [
                        'name' => 'new name',
                    ]
                ],
                $this->testImageFile
            );

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });
    });

    $this->it('returns a 404 status code if the category is missing', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->updateCategory(
            $apiToken,
            200,
            [
                'category' => [
                    'name' => 'new name',
                ]
            ],
            $this->testImageFile
        );

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(404);
    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->updateCategory(
            $apiToken,
            2,
            [
                'category' => [
                    'name' => 'new name',
                ]
            ],
            $this->testImageFile
        );

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns the category updated', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->updateCategory(
            $apiToken,
            2,
            [
                'category' => [
                    'name' => 'new name',
                ]
            ],
            $this->testImageFile
        );

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                'id' => 2,
                'name' => 'new name',
                'icon' => '/images/categories/test-image.png',
            ]
        ]);
    });

    $this->it('updates the category in the database', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->updateCategory(
            $apiToken,
            2,
            [
                'category' => [
                    'name' => 'new name',
                ]
            ],
            $this->testImageFile
        );

        $category = $this->categoriesRepository->find(2);

        $this->expect($category) ->to() ->be() ->like([
            'getId()' => 2,
            'getName()' => 'new name',
            'getIcon()' => 'test-image.png',
        ]);
    });

    $this->describe('with invalid parameters', function () {

        $this->it('fails when the name is missing', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateCategory(
                $apiToken,
                2,
                [
                    'category' => [
                    ]
                ],
                $this->testImageFile
            );

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'Key name must be present'
                ]
            ]);
        });

        $this->it('fails when the name is empty', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateCategory(
                $apiToken,
                2,
                [
                    'category' => [
                        'name' => '',
                    ]
                ],
                $this->testImageFile
            );

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must not be optional'
                ]
            ]);
        });

        $this->it('fails when the name is blank', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateCategory(
                $apiToken,
                2,
                [
                    'category' => [
                        'name' => '   ',
                    ]
                ],
                $this->testImageFile
            );

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must not be blank'
                ]
            ]);
        });

        $this->it('fails when the name is too long', function() {

            $apiToken = $this->loginWithDefaultUser();

            $this->updateCategory(
                $apiToken,
                2,
                [
                    'category' => [
                        'name' => str_repeat('a', 256),
                    ]
                ],
                $this->testImageFile
            );

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must have a length between 1 and 255'
                ]
            ]);
        });

    });
});