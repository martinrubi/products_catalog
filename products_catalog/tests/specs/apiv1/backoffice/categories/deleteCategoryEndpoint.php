<?php
declare(strict_types=1);

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;

$spec->describe('When deleting a category using the endpoint "/apiv1/admin/categories/{category_id}"', function() {

    $this->beforeEach(function (){
        $this->resetDatabase();

        $category_1 = $this->categoriesRepository->persist(
            new Category('Category 1', 'image.png')
        );

        $this->categoriesRepository->persist(
            new Category('Category 2', 'image.png')
        );

        $this->productsRepository->persist(
            new Product('Product 1', 'test-image.png', $category_1)
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->def('deleteCategory', function ($apiToken, $categoryId) {
        $this->request('DELETE',
            "/apiv1/admin/categories/$categoryId",
            [], [],
            [
                'HTTP_X-Auth-Token' => $apiToken
            ]
        );
    });

    $this->describe('with an invalid api token', function (){

        $this->it('returns a 403 status code', function() {

            $this->deleteCategory(null, 2);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });

    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteCategory($apiToken, 2);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns the category deleted', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteCategory($apiToken, 2);

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                'id' => null,
                'name' => 'Category 2',
                'icon' => '/images/categories/',
            ]
        ]);
    });

    $this->it('deletes the category from the database', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteCategory($apiToken, 2);

        $categories = $this->categoriesRepository->findAll();

        $this->expect(count($categories)) ->to() ->equal(1);
        $this->expect($categories) ->to() ->be() ->like([
            [
                'getId()' => 1
            ]
        ]);

    });

    $this->it('returns 404 for an invalid category', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteCategory($apiToken, 200);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(404);

    });

    $this->it('fails if the category is used in products', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteCategory($apiToken, 1);

        $this->expect($this->getJsonResponse()) ->to() ->equal([
            'success' => false,
            'errors' => [
                'name' => 'The category has products.',
            ]
        ]);

    });

});