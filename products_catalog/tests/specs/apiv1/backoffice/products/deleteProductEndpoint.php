<?php
declare(strict_types=1);

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;

$spec->describe('When deleting a product using the endpoint "/apiv1/admin/products/{product_id}"', function() {

    $this->beforeEach(function (){
        $this->resetDatabase();

        $category_1 = $this->categoriesRepository->persist(
            new Category('Category 1', 'icon.png')
        );

        $category_2 = $this->categoriesRepository->persist(
            new Category('Category 2', 'icon.png')
        );

        $this->productsRepository->persist(
            new Product('Product 1', 'test-image.png', $category_1)
        );
        $this->productsRepository->persist(
            new Product('Product 2', 'test-image.png', $category_1)
        );
        $this->productsRepository->persist(
            new Product('Product 3', 'test-image.png', $category_2)
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->def('deleteProduct', function ($apiToken, $productId) {
        $this->request('DELETE',
            "/apiv1/admin/products/$productId",
            [], [],
            [
                'HTTP_X-Auth-Token' => $apiToken
            ]
        );
    });

    $this->describe('with an invalid api token', function (){

        $this->it('returns a 403 status code', function() {

            $this->deleteProduct(null, 2);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });

    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteProduct($apiToken, 2);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns the product deleted', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteProduct($apiToken, 2);

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                'id' => null,
                'name' => 'Product 2',
                'image' => '/images/products/',
                'category' => [
                    'id' => 1,
                    'name' => 'Category 1',
                    'icon' => '/images/categories/icon.png',
                ],
            ],
        ]);
    });

    $this->it('deletes the product from the database', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteProduct($apiToken, 2);

        $products = $this->productsRepository->findAll();

        $this->expect(count($products)) ->to() ->equal(2);
        $this->expect($products) ->to() ->be() ->like([
            [
                'getId()' => 1
            ],
            [
                'getId()' => 3
            ]
        ]);

    });

    $this->it('returns 404 for an invalid product', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->deleteProduct($apiToken, 200);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(404);

    });

});