<?php
declare(strict_types=1);

use AppBundle\Entity\Category;

$spec->describe('When creating a product using the endpoint "/apiv1/admin/products/create"', function() {

    $this->beforeEach(function (){
        $this->resetDatabase();

        $this->categoriesRepository->persist(
            new Category('Category 1', 'icon.png')
        );

        $this->categoriesRepository->persist(
            new Category('Category 2', 'icon.png')
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->def('createProduct', function ($apiToken, $bodyData, $imageFile=null) {
        $this->request('POST',
            '/apiv1/admin/products/create',
            $bodyData,
            ($imageFile ? [ 'productImage' => $this->testImageFile ] : []),
            ['HTTP_X-Auth-Token' => $apiToken]
        );
    });

    $this->describe('with an invalid api token', function (){

        $this->it('returns a 403 status code', function() {

            $this->createProduct(null, [
                'product' => [
                    'name' => 'new name',
                    'categoryId' => 2,
                ]
            ], $this->testImageFile);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });

    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createProduct($apiToken, [
            'product' => [
                'name' => 'new name',
                'categoryId' => 2,
            ]
        ], $this->testImageFile);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns the product created', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createProduct($apiToken, [
            'product' => [
                'name' => 'new name',
                'categoryId' => 2,
            ]
        ], $this->testImageFile);

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                'id' => 1,
                'name' => 'new name',
                'image' => '/images/products/test-image.png',
                'category' => [
                    'id' => 2,
                    'name' => 'Category 2',
                    'icon' => '/images/categories/icon.png',
                ],
            ]
        ]);
    });

    $this->it('creates the product in the database', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->createProduct($apiToken, [
            'product' => [
                'name' => 'new name',
                'categoryId' => 2,
            ]
        ], $this->testImageFile);

        $product = $this->productsRepository->find(1);

        $this->expect($product) ->to() ->be() ->exactlyLike([
            'getId()' => 1,
            'getName()' => 'new name',
            'getImage()' => 'test-image.png',
            'getCategory()' => [
                'getId()' => 2,
                'getName()' => 'Category 2',
            ]
        ]);
    });

    $this->describe('with invalid parameters', function () {

        $this->let( 'validParams', function () {
            return [
                'product' => [
                    'name' => 'new name',
                    'categoryId' => 2,
                ]
            ];
        });

        $this->it('fails when the name is missing', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            unset($params['product']['name']);

            $this->createProduct($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'Key name must be present'
                ]
            ]);
        });

        $this->it('fails when the name is empty', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['name'] = '';

            $this->createProduct($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must not be optional'
                ]
            ]);
        });

        $this->it('fails when the name is blank', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['name'] = '  ';

            $this->createProduct($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must not be blank'
                ]
            ]);
        });

        $this->it('fails when the name is too long', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['name'] = str_repeat('a', 256);

            $this->createProduct($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must have a length between 1 and 255'
                ]
            ]);
        });

        $this->it('fails when the category is missing', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            unset($params['product']['categoryId']);

            $this->createProduct($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'categoryId' => 'Key categoryId must be present'
                ]
            ]);
        });

        $this->it('fails when the category is empty', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['categoryId'] = '';

            $this->createProduct($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'categoryId' => 'categoryId must not be optional'
                ]
            ]);
        });

        $this->it('fails when the category does not exist', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['categoryId'] = '200';

            $this->createProduct($apiToken, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'categoryId' => 'The categoryId was not found.'
                ]
            ]);
        });

    });
});