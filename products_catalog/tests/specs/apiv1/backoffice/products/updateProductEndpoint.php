<?php
declare(strict_types=1);

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;

$spec->describe('When updating a product using the endpoint "/apiv1/admin/products/{product_id}"', function() {

    $this->beforeEach(function (){
        $this->resetDatabase();

        $category_1 = $this->categoriesRepository->persist(
            new Category('Category 1', 'icon.png')
        );

        $category_2 = $this->categoriesRepository->persist(
            new Category('Category 2', 'icon.png')
        );

        $this->productsRepository->persist(
            new Product('Product 1', 'test-image.png', $category_1)
        );
        $this->productsRepository->persist(
            new Product('Product 2', 'test-image.png', $category_1)
        );
        $this->productsRepository->persist(
            new Product('Product 3', 'test-image.png', $category_2)
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->def('updateProduct', function ($apiToken, $productId, $bodyData, $imageFile=null) {
        $this->request('POST',
            "/apiv1/admin/products/$productId",
            $bodyData,
            ($imageFile ? [ 'productImage' => $this->testImageFile ] : []),
            ['HTTP_X-Auth-Token' => $apiToken]
        );
    });

    $this->describe('with an invalid api token', function (){

        $this->it('returns a 403 status code', function() {

            $this->updateProduct(null, 2, [
                'product' => [
                    'name' => 'new name',
                    'categoryId' => 2,
                ]
            ], $this->testImageFile);

            $this->expect($this->getResponseStatusCode()) ->to() ->equal(403);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => ['Invalid credentials.']
            ]);
        });

    });

    $this->it('returns a 200 status code', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->updateProduct($apiToken, 2, [
            'product' => [
                'name' => 'new name',
                'categoryId' => 2,
            ]
        ], $this->testImageFile);

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns the product created', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->updateProduct($apiToken, 2, [
            'product' => [
                'name' => 'new name',
                'categoryId' => 2,
            ]
        ], $this->testImageFile);

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                'id' => 2,
                'name' => 'new name',
                'image' => '/images/products/test-image.png',
                'category' => [
                    'id' => 2,
                    'name' => 'Category 2',
                    'icon' => '/images/categories/icon.png',
                ],
            ]
        ]);
    });

    $this->it('creates the product in the database', function() {

        $apiToken = $this->loginWithDefaultUser();

        $this->updateProduct($apiToken, 2, [
            'product' => [
                'name' => 'new name',
                'categoryId' => 2,
            ]
        ], $this->testImageFile);

        $product = $this->productsRepository->find(2);

        $this->expect($product) ->to() ->be() ->exactlyLike([
            'getId()' => 2,
            'getName()' => 'new name',
            'getImage()' => 'test-image.png',
            'getCategory()' => [
                'getId()' => 2,
                'getName()' => 'Category 2',
            ]
        ]);
    });

    $this->describe('with invalid parameters', function () {

        $this->let( 'validParams', function () {
            return [
                'product' => [
                    'name' => 'new name',
                    'categoryId' => 2,
                ]
            ];
        });

        $this->it('fails when the name is missing', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            unset($params['product']['name']);

            $this->updateProduct($apiToken, 2, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'Key name must be present'
                ]
            ]);
        });

        $this->it('fails when the name is empty', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['name'] = '';

            $this->updateProduct($apiToken, 2, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must not be optional'
                ]
            ]);
        });

        $this->it('fails when the name is blank', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['name'] = '  ';

            $this->updateProduct($apiToken, 2, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must not be blank'
                ]
            ]);
        });

        $this->it('fails when the name is too long', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['name'] = str_repeat('a', 256);

            $this->updateProduct($apiToken, 2, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'name' => 'name must have a length between 1 and 255'
                ]
            ]);
        });

        $this->it('fails when the category is missing', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            unset($params['product']['categoryId']);

            $this->updateProduct($apiToken, 2, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'categoryId' => 'Key categoryId must be present'
                ]
            ]);
        });

        $this->it('fails when the category is empty', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['categoryId'] = '';

            $this->updateProduct($apiToken, 2, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'categoryId' => 'categoryId must not be optional'
                ]
            ]);
        });

        $this->it('fails when the category does not exist', function() {

            $apiToken = $this->loginWithDefaultUser();

            $params = $this->validParams;

            $params['product']['categoryId'] = '200';

            $this->updateProduct($apiToken, 2, $params);

            $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
                'success' => false,
                'errors' => [
                    'categoryId' => 'The categoryId was not found.'
                ]
            ]);
        });

    });
});