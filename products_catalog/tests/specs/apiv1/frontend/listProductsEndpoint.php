<?php
declare(strict_types=1);

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;

$spec->describe('When getting the products from the endpoint "/apiv1/products"', function() {

    $this->beforeAll(function (){
        $this->resetDatabase();

        $category_1 = $this->categoriesRepository->persist(
            new Category('Category 1', 'icon.png')
        );

        $category_2 = $this->categoriesRepository->persist(
            new Category('Category 2', 'icon.png')
        );

        $this->productsRepository->persist(
            new Product('Product 1', 'test-image.png', $category_1)
        );
        $this->productsRepository->persist(
            new Product('Product 2', 'test-image.png', $category_1)
        );
        $this->productsRepository->persist(
            new Product('Product 3', 'test-image.png', $category_2)
        );

        $this->em->flush();

        $this->em->clear();
    });

    $this->def('listProducts', function($q=null) {
        $this->request('GET', "/apiv1/products?q=$q");
    });

    $this->it('returns a 200 status code', function() {

        $this->listProducts();

        $this->expect($this->getResponseStatusCode()) ->to() ->equal(200);
    });

    $this->it('returns all the products', function() {

        $this->listProducts();

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                [
                    'id' => 1,
                    'name' => 'Product 1',
                    'image' => '/images/products/test-image.png',
                    'category' => [
                        'id' => 1,
                        'name' => 'Category 1',
                        'icon' => '/images/categories/icon.png',
                    ],
                ],
                [
                    'id' => 2,
                    'name' => 'Product 2',
                    'image' => '/images/products/test-image.png',
                    'category' => [
                        'id' => 1,
                        'name' => 'Category 1',
                        'icon' => '/images/categories/icon.png',
                    ],
                ],
                [
                    'id' => 3,
                    'name' => 'Product 3',
                    'image' => '/images/products/test-image.png',
                    'category' => [
                        'id' => 2,
                        'name' => 'Category 2',
                        'icon' => '/images/categories/icon.png',
                    ],
                ],
            ],
        ]);
    });

    $this->it('returns the products matching a term', function() {

        $this->listProducts('3');

        $this->expect($this->getJsonResponse()) ->to() ->be() ->exactlyLike([
            'success' => true,
            'data' => [
                [
                    'id' => 3,
                    'name' => 'Product 3',
                    'image' => '/images/products/test-image.png',
                    'category' => [
                        'id' => 2,
                        'name' => 'Category 2',
                        'icon' => '/images/categories/icon.png',
                    ],
                ],
            ],
        ]);
    });

});