CREATE USER 'catalog_user'@'%' IDENTIFIED BY '123456';
CREATE DATABASE IF NOT EXISTS `catalog_test`;
CREATE DATABASE IF NOT EXISTS `catalog_dev`;
GRANT ALL PRIVILEGES ON `catalog_test`.* TO 'catalog_user'@'%';
GRANT ALL PRIVILEGES ON `catalog_dev`.* TO 'catalog_user'@'%';