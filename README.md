Products Catalog
========================

This is a sample single page application implemented with `React` + `Symfony 3.2`.

I did it as an exercise to apply to a position but also to learn `React`.
This is the first frontend single page application I've implemented so far.
I liked `React` a lot.

### Integration branch

This branch is a proof of concept of the integration of the library [haijin/specs](https://github.com/haijin-development/php-specs)
in a Symfony application.

[Here](https://bitbucket.org/martinrubi/products_catalog/src/master/) you can browse the master branch using the regular libraries.

Note that it's possible for the original `PHPUnit` tests to coexist with [haijin/specs](https://github.com/haijin-development/php-specs) specs with no conflicts.

### Requirements

The application was developed and tested in `Ubuntu 18` with `PHP 7.2`, the last version of the `Chrome` browser and uses the following:

- `Vagrant` for setting up the development environment.
- `Symfony 3.2` (that specific version was a requirement of the exercise).
- `Mysql` + `Doctrine`.
- `Redis` for storing the session API tokens.
- `PHPUnit` for testing the backend.
- The last version of [nodejs](https://nodejs.org/en/) + [React](https://reactjs.org/) as a frontend framework.
-  [Materialize](https://materializecss.com/) as a responsive/grid/styling/widgets library.

To see the full list of the application requirements see the folder

[vagrant/provision](vagrant/provision)

The application is fully functional and the backend has a good tests coverage.

Many things can be improved though but I did not want to extend the delivery of the exercise for more than a week.

This is a list of things the application is missing:

- React tests
- Pagination
- Filtering lists in the admin side.
- The use of an images framework (like imagemagick) to resize the uploaded images before storing them.
- User roles to have a super user role that could edit/delete other users.

This is a list of things that may be improved:

- Switch from Vagrant to Docker. I like Vagrant though.
- Use a Symfony framework like FOS to implement some API tasks. I used FOS a few years ago and I did not like it much, may be I should give it another try.
- Use a Symfony framework to handle the signup/sigin of users (like logging login attempts, reset passwords, etc). I did the bare minimum to have a working application.
- Research for a [Materialize](https://materializecss.com/) framework for React.
- Use `React` Context to pass the session data along its components.
- Use `ReactRedux` to implement the components logic.

# Developing

To have a fully functional development environment up and running follow these steps:

### Clone the repository

Open a console terminal and execute all of the following commands, always in the same terminal, in the given order:

```
git clone https://martinrubi@bitbucket.org/martinrubi/products_catalog.git
```

### Start Vagrant

```
cd products_catalog/vagrant
vagrant up
```

### Go to the project directory in the Vagrant machine

```
vagrant ssh
cd products_catalog/
```

### Install PHP dependencies

```
composer install
```

### Install Node dependencies

```
npm install
```

### Prepare the database for testing

```
php bin/console doctrine:database:drop --force --env=test
php bin/console doctrine:database:create --env=test
php bin/console doctrine:migration:migrate --env=test
```

### Prepare the database for development

```
php bin/console doctrine:database:drop --force --env=dev
php bin/console doctrine:database:create --env=dev
php bin/console doctrine:migration:migrate --env=dev
```

### Compile the assets

```
./node_modules/.bin/encore dev
```

### Run PHP tests

```
composer test
```

### Run the server

```
php bin/console server:run 192.168.33.10:8080
```

### Run webpack encore to compile assets when they change

I another terminal do

```
cd products_catalog/vagrant
vagrant ssh
cd products_catalog/
./node_modules/.bin/encore dev --watch
```

### Browse the application

Wait a few seconds for the servers to start and in a browser go to the page

http://192.168.33.10:8080/

The initial user is:

```
username: admin
password: 123456
```